


export default initializer => {

   let collection;

   const testFilter = async (filter, expectedResults) => {
      const results = await collection.find(filter);

      expect( results.map(item => item.id) ).toEqual( expectedResults  );
   };


   beforeAll(async () => {
      collection = await initializer();
   });

   beforeEach(async () => {
      await collection.remove({});
   });


   describe('standard queries', () => {

      beforeEach(async () => {
         await collection.upsert({ id:'1', a:'Alice', b:1, c: { d: 1, e: 2 } });
         await collection.upsert({ id:'2', a:'Charlie', b:2, c: { d: 2, e: 3 } });
         await collection.upsert({ id:'3', a:'Bob', b:3 });
      });


      it('should find all rows', async () => testFilter({}, ['1', '2', '3']));

      it('should search by id', async () => testFilter({ id: '1' }, ['1']));

      it('should search by string', async () => testFilter({ a: 'Alice' }, ['1']));

      it('should search $in string', async () => testFilter({ a: { $in: ['Alice', 'Charlie'] } }, ['1', '2']));

      it('should search by number', async () => testFilter({ b: 2 }, ['2']));

      it('should search $in number', async () => testFilter({ b: { $in: [2, 3] } }, ['2', '3']));

      it('should search by regexp', async () => testFilter({ a: { $regex: 'li'} }, ['1', '2']));

      it('should search by regexp with case sensitive', async () => testFilter({ a: { $regex: 'A'} }, ['1']));

      it('should search by regexp with case insensitive', async () => testFilter({ a: { $regex: 'A', $options: 'i'} }, ['1', '2']));

      it('should search by $or', async () => testFilter({ '$or': [{b:1}, {b:2}]}, ['1', '2']));

      it('should search by path', async () => testFilter({ 'c.d': 2 }, ['2']));

      it('should search by $ne', async () => testFilter({ 'b': { $ne: 2 }}, ['1', '3']));

      it('should search by $gt', async () => testFilter({ 'b': { $gt: 1 }}, ['2', '3']));

      it('should search by $lt', async () => testFilter({ 'b': { $lt: 3 }}, ['1', '2']));

      it('should search by $gte', async () => testFilter({ 'b': { $gte: 2 }}, ['2', '3']));

      it('should search by $lte', async () => testFilter({ 'b': { $lte: 2 }}, ['1', '2']));

      it('should search by $not', async () => testFilter({ 'b': { $not: { $lt: 3 }}}, ['3']));

      it('should search by $or', async () => testFilter({ $or: [{b: 3},{b: 1}]}, ['1', '3']));

      it('should search by $exists: true', async () => testFilter({ c: { $exists: true }}, ['1', '2']));

      it('should search by $exists: false', async () => testFilter({ c: { $exists: false }}, ['3']));


      it('should include fields', async () => {
         const results = await collection.find({ id: '1' }, { fields: { a: 1 }});

         expect( results[0] ).toEqual({ id: '1', a: 'Alice' });
      });

      it('should include subfields', async () => {
         const results = await collection.find({ id: '1' }, { fields: { 'c.d': 1 }});

      expect( results[0] ).toEqual({ id: '1',  c: { d: 1 } });
      });

      it('should ignore non-existent subfields', async () => {
         const results = await collection.find({ id: '1' }, { fields: { 'x.y': 1 }});

         expect( results[0] ).toEqual({ id: '1' });
      });

      it('should excludes fields', async () => {
         const results = await collection.find({ id: '1' }, { fields: { a: 0 }});

         expect( results[0].a ).toBeUndefined();
         expect( results[0].b ).toBe(1);
      });

      it('should exclude subfields', async () => {
         const results = await collection.find({ id: '1' }, { fields: {  'c.d': 0 }});

         expect( results[0].c ).toEqual({ e: 2 });
      });

      it('should find one row', async () => {
         const result = await collection.findOne({ id: '2' });
         
         expect( result.a ).toBe('Charlie');
      });

      it('should remove item', async () => {
         await collection.remove('2');

         const results = await collection.find({});

         expect( results ).toHaveLength(2);

         expect( results.some(item => item.id === '1') ).toBeTruthy();
         expect( results.some(item => item.id === '2') ).toBeFalsy();
      });

      it('should remove non-existent items', async () => {
         await collection.remove('999');
         
         const results = await collection.find({});

         expect( results ).toHaveLength(3);
      });

      it('should sort ascending', async () => {
         const results = await collection.find({}, { sort: ['a']});

         expect( results.map(item => item.id) ).toEqual( ['1', '3', '2']  );
      });

      it('should sort descending', async () => {
         const results = await collection.find({}, {sort: [['a','desc']]});
         
         expect( results.map(item => item.id) ).toEqual( ['2', '3', '1']  );
      });

      it('should limit', async () => {
         const results = await collection.find({}, { sort: ['a'], limit: 2 });
         
         expect( results.map(item => item.id) ).toEqual( ['1', '3'] );
      });

      it('should skip', async () => {
         const results = await collection.find({}, { sort: ['a'], skip: 2 });
         
         expect( results.map(item => item.id) ).toEqual( ['2']  );
      });

      it('should fetch independent copies', async () => {
         const result1 = await collection.findOne({}, { sort: ['a'], skip: 2 });
         const result2 = await collection.findOne({}, { sort: ['a'], skip: 2 });

         expect( result1 ).toEqual( result2 );
         expect( result1 ).not.toBe( result2 );
      });

      it('should keep independent copies through upsert', async () => {
         const doc = { id: '2' };
         
         const item = await collection.upsert(doc);

         doc.a = 'xyz';
         item.a = 'xyz';

         const doc2 = await collection.findOne({ id: '2' });
         
         expect( doc ).not.toBe( doc2 );
         expect( doc2.a ).not.toBe( doc.a );
      });

      it('should create id field', async () => {
         const item = await collection.upsert({ a: '1' });
         
         expect( item.hasOwnProperty('id') ).toBeTruthy();
         expect( item.id ).toHaveLength(32);
      });

      it('should return array if upsert multiple documents', async () => {
         const items = await collection.upsert([{ a: '1' }]);

         expect( items ).toHaveLength(1);
         expect( items.map(item => item.a) ).toEqual( ['1'] );
      });

      it('should update by id', async () => {
         await collection.upsert({ id: '1', a: '1' });
         await collection.upsert({ id: '1', a: '2', b: 1 });

         const items = await collection.find({ id: '1' });

         expect( items ).toHaveLength(1);
         expect( items[0].a ).toBe('2');
         expect( items[0].b ).toBe(1);
      });

      it('should return upserted row', async () => {
         const item = await collection.upsert({ id: '1', a: '1' });

         expect( item.id ).toBe('1');
         expect( item.a ).toBe('1');
      });

   });


   describe('with batch', () => {

      it('should upsert multiple rows', async () => {
         const docs = [];

         for (let i = 0; i < 100; ++i) {
            docs.push({ b: i });
         }

         await collection.upsert(docs);

         const results = await collection.find({});

         expect( results ).toHaveLength(100);
      });

   });


   describe('with sample with capitalization', () => {

      beforeEach(async () => {
         await collection.upsert({ id:'1', a:'Alice', b:1, c: { d: 1, e: 2 } });
         await collection.upsert({ id:'2', a:'AZ', b:2, c: { d: 2, e: 3 } });
      });


      it('should sort fields in JavaScript order', async () => {
         const results = await collection.find({}, { sort: ['a'] });

         expect( results.map(item => item.id) ).toEqual(['2', '1']);
      });

   });


   describe('with integer array in JSON rows', () => {
      
      beforeEach(async () => {
         await collection.upsert({ id:'1', c: { arrint: [1, 2] }});
         await collection.upsert({ id:'2', c: { arrint: [2, 3] }});
         await collection.upsert({ id:'3', c: { arrint: [1, 3] }});
      });
      
      it('should filter by $in', async () => testFilter({ 'c.arrint': { $in: [3] }}, ['2', '3']));

      it('should filter by list $in with multiple', async () => testFilter({ 'c.arrint': { $in: [1, 3] }}, ['1', '2', '3']));
      
   });


   describe('with object array rows', () => {

      beforeEach(async () => {
         await collection.upsert({ id:'1', c: [{ x: 1, y: 1 }, { x:1, y:2 }] });
         await collection.upsert({ id:'2', c: [{ x: 2, y: 1 }] });
         await collection.upsert({ id:'3', c: [{ x: 2, y: 2 }] });
      });


      it('should filter by elemMatch', async () => {
         await testFilter({ 'c': { $elemMatch: { y:1 }}}, ['1', '2']);
         await testFilter({ 'c': { $elemMatch: { x:1 }}}, ['1']);
      });

   });


   describe('with array rows with inner string arrays', () => {

      beforeEach(async () => {
         await collection.upsert({ id:'1', c: [{ arrstr: ['a', 'b']}, { arrstr: ['b', 'c']}] });
         await collection.upsert({ id:'2', c: [{ arrstr: ['b']}] });
         await collection.upsert({ id:'3', c: [{ arrstr: ['c', 'd']}, { arrstr: ['e', 'f']}] });
      });


      it('should filter by $elemMatch', async () => {
         await testFilter({ 'c': { $elemMatch: { 'arrstr': { $in: ['b']} }}}, ['1', '2']);
         await testFilter({ 'c': { $elemMatch: { 'arrstr': { $in: ['d', 'e']} }}}, ['3'])
      });

   });


   describe('with text array rows', () => {

      beforeEach(async () => {
         await collection.upsert({ id:'1', textarr: ['a', 'b']});
         await collection.upsert({ id:'2', textarr: ['b', 'c']});
         await collection.upsert({ id:'3', textarr: ['c', 'd']});
      });


      it('should filter by $in', async () => testFilter({ 'textarr': { $in: ['b'] }}, ['1', '2']));

      it('should filter by direct reference', async () => testFilter({ 'textarr': 'b' }, ['1', '2']));

      it('should filter by both item and complete array', async () => testFilter({ 'textarr': { $in: ['a', ['b', 'c']] } }, ['1', '2']));

   });


   describe('with geolocated rows', () => {

      const geopoint = (lng, lat) => ({ type: 'Point', coordinates: [lng, lat] });

      beforeEach(async () => {
         await collection.upsert({ id:'1', geo: geopoint(90, 45) });
         await collection.upsert({ id:'2', geo: geopoint(90, 46) });
         await collection.upsert({ id:'3', geo: geopoint(91, 45) });
         await collection.upsert({ id:'4', geo: geopoint(91, 46) });
      });


      it('should find points near', async () => {
         const selector = { geo: { $near: { $geometry: geopoint(90, 45) } } };

         const results = await collection.find(selector);

         expect( results.map(item => item.id) ).toEqual( ['1', '3', '2', '4'] );
      });

      it('should find points near maxDistance', async () => {
         const selector = { geo: {
            $near: {
               $geometry: geopoint(90, 45),
               $maxDistance: 111180
            }
         } };

         const results = await collection.find(selector);
         
         expect( results.map(item => item.id) ).toEqual( ['1', '3'] );
      });

      it('should find points near maxDistance just above', async () => {
         const selector = { geo: {
            $near: {
               $geometry: geopoint(90, 45),
               $maxDistance: 111410
            }
         } };

         const results = await collection.find(selector);
         
         expect( results.map(item => item.id) ).toEqual( ['1', '3', '2'] );
      });

      it('should find points within simple box', async () => {
         const selector = { geo: {
            $geoIntersects: {
               $geometry: {
                  type: 'Polygon',
                  coordinates: [[
                     [89.5, 45.5], [89.5, 46.5], [90.5, 46.5], [90.5, 45.5], [89.5, 45.5]
                  ]]
               }
            }
         } };

         const results = await collection.find(selector);
         
         expect( results.map(item => item.id) ).toEqual( ['2'] );
      });

      it('should find points within big box', async () => {
         const selector = { geo: {
            $geoIntersects: {
               $geometry: {
                  type: 'Polygon',
                  coordinates: [[
                     [0, -89], [0, 89], [179, 89], [179, -89], [0, -89]
                  ]]
               }
            }
         } };

         const results = await collection.find(selector);
         
         expect( results.map(item => item.id) ).toEqual( ['1', '2', '3', '4'] );
      });

      it('should handle undefined', async () => {
         const selector = { geo: {
            $geoIntersects: {
               $geometry: {
                  type: 'Polygon',
                  coordinates: [[
                     [89.5, 45.5], [89.5, 46.5], [90.5, 46.5], [90.5, 45.5], [89.5, 45.5]
                  ]]
               }
            }
         } };

         await collection.upsert({ id:5 });

         const results = await collection.find(selector);
         
         expect( results.map(item => item.id) ).toEqual( ['2'] );
      });

   });


   describe('with polygon rows', () => {

      const polygon = coordinates => ({ type: 'Polygon', coordinates });

      beforeEach(async () => {
         await collection.upsert({ id:'1', geo: polygon([[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]]]) });
         await collection.upsert({ id:'2', geo: polygon([[[10, 10], [11, 10], [11, 11], [10, 11], [10, 10]]]) });
      });


      it('should find polygons that intersect simple box', async () => {
         const selector = { geo: {
            $geoIntersects: {
               $geometry: polygon([[[0, 0], [2, 0], [2, 2], [0, 2], [0, 0]]])
            }
         } };

         const results = await collection.find(selector);
         
         expect( results.map(item => item.id) ).toEqual( ['1'] );
      });

      it('should find polygon intersecting large box', async () => {
         const selector = { geo: {
            $geoIntersects: {
               $geometry: polygon([[[0, 0], [12, 0], [12, 12], [0, 12], [0, 0]]])
            }
         } };

         const results = await collection.find(selector);
         
         expect( results.map(item => item.id) ).toEqual( ['1', '2'] );
      });

   })


   describe('With multipolygon rows', () => {

      const polygon = coordinates => ({ type: 'Polygon', coordinates });

      const multipolygon = coordinates => ({ type: 'MultiPolygon', coordinates });

      beforeEach(async () => {
         await collection.upsert({ id:'1', geo: multipolygon([[[[0, 0], [1, 0], [1, 1], [0, 1], [0, 0]]]]) });
         await collection.upsert({ id:'2', geo: multipolygon([[[[10, 10], [11, 10], [11, 11], [10, 11], [10, 10]]]]) });
      });


      it('should find polygons that intersect simple box', async () => {
         const selector = { geo: {
            $geoIntersects: {
               $geometry: polygon([[[0, 0], [2, 0], [2, 2], [0, 2], [0, 0]]])
            }
         } };

         const results = await collection.find(selector);
         
         expect( results.map(item => item.id) ).toEqual( ['1'] );
      });

      it('should find polygons that intersect large box', async () => {
         const selector = { geo: {
            $geoIntersects: {
               $geometry: polygon([[[0, 0], [12, 0], [12, 12], [0, 12], [0, 0]]])
            }
         } };

         const results = await collection.find(selector);
         
         expect( results.map(item => item.id) ).toEqual( ['1', '2'] );
      });

   });


   describe('With multilinestring rows', () => {
      
      const polygon = coordinates => ({ type: 'Polygon', coordinates });

      beforeEach(async () => {
         const linestring = {
            type: 'MultiLineString',
            coordinates: [
              [[0, 0], [0, 1]],
              [[0, 0], [1, 0]]
            ]
         };

         await collection.upsert({ id:"1", geo: linestring });
      });


      it('should find that it intersect simple box', async () => {
         const selector = { geo: {
            $geoIntersects: {
               $geometry: polygon([[[0, 0], [2, 0], [2, 2], [0, 2], [0, 0]]])
            }
         } };

         const results = await collection.find(selector);
         
         expect( results.map(item => item.id) ).toEqual( ['1'] );
      });

      it('should find that it does not intersect simple box', async () => {
         const selector = { geo: {
            $geoIntersects: {
               $geometry: polygon([[[2, 2], [3, 2], [3, 3], [2, 3], [2, 2]]])
            }
         } };

         const results = await collection.find(selector);
         
         expect( results.map(item => item.id) ).toEqual( [] );
      });

   });

};