import RemoteDb from '../src/RemoteDb';




describe('Testing IndexedDb', () => {


   const createHttpClient = (expected, response) => (method, url, params, body, options) => {
      if (expected.method) {
         expect( method ).toBe( expected.method );
      }
      if (expected.url) {
         expect( url ).toBe( expected.url );
      }
      if (expected.params) {
         expect( params ).toEqual( expected.params );
      }
      if (expected.body) {
         expect( body ).toEqual( expected.body );
      }
      if (expected.options) {
         expect( options ).toEqual( expected.options );
      }

      return response;
   };



   it('should create instance and collection', async () => {
      const db = new RemoteDb();

      await db.addCollection('tests');

      expect( db.tests ).toBeTruthy();
            
   });


   it('should save data remotely', async () => {
      const model = { id: 1, name: 'foo' };
      const url = 'test.domain';
      const clientName =  'test-01';
      const httpClient = createHttpClient({
         url: `${url}/tests`,
         method: 'POST',
         params: { client: clientName }
      }, model);

      const db = new RemoteDb({
         url: url,
         client: clientName,
         httpClient: httpClient
      });

      await db.addCollection('tests');

      const doc = await db.tests.upsert( model );

      expect( doc.name ).toBe('foo');
   });



   it('should retrieve one row remotely', async () => {
      const model = { id: 1, name: 'foo' };
      const url = 'test.domain';
      const clientName =  'test-02';
      const httpClient = createHttpClient({
         url: `${url}/tests`,
         method: 'GET',
         params: { client: clientName, selector: 1 }
      }, model);

      const db = new RemoteDb({
         url: url,
         client: clientName,
         httpClient: httpClient
      });

      await db.addCollection('tests');

      const otherDoc = await db.tests.findOne(1);

      expect( otherDoc.id ).toBe(1);
      expect( otherDoc.name ).toBe( model.name );

   });


   it('should retrieve multiple rows remotely', async () => {
      const models = [{ id: 1, name: 'foo' }, { id: 2, name: 'Bar' }];
      const url = 'test.domain';
      const clientName =  'test-03';
      const httpClient = createHttpClient({
         url: `${url}/tests`,
         method: 'GET',
         params: { client: clientName },
         body: { selector: {} }
      }, models );

      const db = new RemoteDb({
         url: url,
         client: clientName,
         httpClient: httpClient
      });

      await db.addCollection('tests');

      const results = await db.tests.find();

      expect( results ).toHaveLength(2);
      expect( results[0].id ).toBe(1);
      expect( results[1].id ).toBe(2);

   });


   it('should pass params to the request', async () => {

      const url = 'test.domain';
      const params = { a: 'foo' };
      const clientName = 'testParams';
      const httpClient = createHttpClient({
         url: `${url}/tests`,
         method: 'GET',
         params: { client: clientName, ...params },
         body: { selector: {} }
      }, [] );

      const db = new RemoteDb({
         url: url,
         client: clientName,
         httpClient: httpClient
      });


      await db.addCollection('tests');

      const results = await db.tests.find({}, { params });

      expect( results ).toHaveLength(0);

   });


});