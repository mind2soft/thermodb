import MemoryDb from '../src/MemoryDB';

import standardQueries from './standard_queries';
import standardCaching from './standard_caching';



describe('Testing MemoryDb', () => {



   it('should create instance and collection', async () => {

      const db = new MemoryDb();

      await db.addCollection('tests');

      expect( db.tests ).toBeTruthy();

   });


   it('should store and retrive documents', async () => {
      const db = new MemoryDb();

      await db.addCollection('tests');

      const doc = await db.tests.upsert({ id: 1, name: 'foo' });

      expect( doc.name ).toBe('foo');

      const otherDoc = await db.tests.findOne(1);

      expect( otherDoc.id ).toBe(1);
      expect( otherDoc.name ).toBe( doc.name );

      const results = await db.tests.find({ name: 'foo' });

      expect( results ).toHaveLength(1);
      expect( results[0].id ).toBe(1);

   });


   it('should remove item from collection', async () => {
      const db = new MemoryDb();

      await db.addCollection('tests');

      await db.tests.upsert({ id: '124', name: 'Foo' });
      await db.tests.upsert({ id: '200', name: 'Bar' });

      const fooDoc = await db.tests.findOne({ name: 'Foo' });
      const barDoc = await db.tests.findOne({ name: 'Bar' });

      expect( fooDoc.id ).toBe( '124' );
      expect( barDoc.id ).toBe( '200' );

      const deleted = await db.tests.remove({ name: 'Bar' });

      expect( deleted ).toBe(1);

      const oldBarDoc = await db.tests.findOne({ name: 'Bar' });

      expect( oldBarDoc ).toBeFalsy();
      
   }, 200);


   describe('Testing standard quries', () => {
      standardQueries(async () => {
         const db = new MemoryDb();

         await db.addCollection('testStandardQueries');
   
         return db.testStandardQueries;
      });
   });


   describe('Testing standard caching', () => {
      standardCaching(async () => {
         const db = new MemoryDb();

         await db.addCollection('testStandardCaching');
   
         return db.testStandardCaching;
      });
   });

})