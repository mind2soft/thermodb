import http from 'http';

import httpClient, { serializeParams } from '../src/httpClient';


describe('Testing httpClient', () => {

   let server;

   beforeAll(() => {
      server = http.createServer((request, response) => {
         response.writeHead(200, {"Content-Type" : "application/json"});

         if (request.method === 'GET') {
            response.end( JSON.stringify({ a: 'Hello' }) );
         } else {
            let data = '';

            request.on('data', chunk => {
               data = data + chunk;
            });
            request.on('end', () => {
               const body = JSON.parse(data);

               body.server = true;

               response.end( JSON.stringify(body) );
            });
         }
      });
      server.listen(7000);
   });

   afterAll(() => {
      server.close();
   });


   it('should serialize params', () => {
      const params = { foo: 'bar', a: { b: { c: true }}};

      const serialized = serializeParams(params);

      expect( serialized ).toBe('foo=bar&a=%7B%22b%22%3A%7B%22c%22%3Atrue%7D%7D');
   });


   it('should make request', async () => {
      const response = await httpClient('get', 'http://localhost:7000');

      expect(response).toEqual({ a: 'Hello' });
   });

   
   it('should pass POST', async () => {
      const response = await httpClient('post', 'http://localhost:7000', null, { foo: 'bar' });

      expect( response ).toEqual({ foo: 'bar', server: true });
   })

});