import {
   KeyUtil
} from '../src/utils';


describe('Testing Utils', () => {

   describe('KeySerial', () => {

      it('should create no collision keys', () => {
         let prevKey;
         let endTS = Date.now() + (1000 * 3);

         const key = new KeyUtil.Serial();

         while (endTS > Date.now()) {
            const nextKey = key.createKey();

            expect( nextKey ).not.toBe( prevKey );

            prevKey = nextKey;
         }

      }, 4000);

   });


});