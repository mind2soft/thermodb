

export default initializer => {

   let collection;

   // const testFilter = async (filter, expectedResults) => {
   //    const results = await collection.find(filter);

   //    expect( results.map(item => item.id) ).toEqual( expectedResults  );
   // };


   beforeAll(async () => {
      collection = await initializer();
   });

   beforeEach(async () => {
      await collection.resolveUpserts( await collection.pendingUpserts() );
      await collection.remove({});
      await collection.resolveRemoves( await collection.pendingRemoves() );
   });

   
   it('should cache rows', async () => {
      await collection.cache([{ id: '1', a: 'apple' }], {}, {});

      const results = await collection.find({});

      expect( results ).toHaveLength(1);
      expect( results[0].a ).toBe('apple');
   });

   it('should cache rows', async () => {
      const rows = [
         { id: '1', a: 'apple' },
         { id: '2', a: 'banana' },
         { id: '3', a: 'orange' },
         { id: '4', a: 'kiwi' }
      ];

      await collection.cache(rows, {}, {});

      const results = await collection.find({});

      expect( results ).toHaveLength(4);
   });

   it('should cache zero rows', async () => {
      const rows = [];

      await collection.cache(rows, {}, {});

      const results = await collection.find({});

      expect( results ).toHaveLength(0);
   });
   
   it('should overwrite existing items when cached', async () => {
      await collection.cache([ { id: '1', a: 'apple' } ], {}, {});
      await collection.cache([ { id: '1', a: 'banana' } ], {}, {});

      const results = await collection.find({});

      expect( results[0].a ).toBe('banana');
   });

   it('should not overwrite cache if _rev is the same', async () => {
      await collection.cache([ { id: '1', a: 'apple', _rev: 2 } ], {}, {});
      await collection.cache([ { id: '1', a: 'banana', _rev: 2 } ], {}, {});
      
      const results = await collection.find({});

      expect( results[0].a ).toBe('apple');
   });

   it('should overwrite cache if _rev is greater', async () => {
      await collection.cache([ { id: '1', a: 'apple', _rev: 1 } ], {}, {});
      await collection.cache([ { id: '1', a: 'banana', _rev: 2 } ], {}, {});
      
      const results = await collection.find({});

      expect( results[0].a ).toBe('banana');
   });

   it('should not overwrite cache if _rev is the same', async () => {
      await collection.cache([ { id: '1', a: 'apple', _rev: 2 } ], {}, {});
      await collection.cache([ { id: '1', a: 'banana', _rev: 1 } ], {}, {});
      
      const results = await collection.find({});

      expect( results[0].a ).toBe('apple');
   });

   it('should not overwrite upserts', async () => {
      await collection.upsert([ { id: '1', a: 'apple' } ], {}, {});
      await collection.cache([ { id: '1', a: 'banana' } ], {}, {});
      
      const results = await collection.find({});

      expect( results[0].a ).toBe('apple');
   });

   it('should not overwrite remove', async () => {
      await collection.cache([ { id: '1', a: 'delete' } ], {}, {});
      await collection.remove('1');
      await collection.cache([ { id: '1', a: 'banana' } ], {}, {});

      const results = await collection.find({});

      expect( results ).toHaveLength(0);
   });

   it('should remove missing unsorted', async () => {
      await collection.cache([ { id: '1', a: 'a' }, { id: '2', a: 'b' }, { id: '3', a: 'c' }], {}, {});
      await collection.cache([{ id: '1', a: 'a' }, { id: '3', a: 'c' }], {}, {});

      const results = await collection.find({});

      expect( results ).toHaveLength(2);
   });

   it('should exclude excluded', async () => {
      await collection.cache([{ id: '1', a: 'a' }, { id: '2', a: 'b' }, { id: '3', a: 'c' }], {}, {});
      await collection.cache([{ id: '1', a: 'a' }, { id: '4', a: 'd' }, { id: '5', a: 'e' }], {}, { exclude: ['2', '4'] });

      const results = await collection.find({}, { sort: ['id'] });

      expect( results.map(result => result.id) ).toEqual(['1', '2', '5']);
   });

   it('should remove missing filtered', async () => {
      await collection.cache([{ id: '1', a: 'a' }, { id: '2', a: 'b' }, { id: '3', a: 'c' }], {}, {});
      await collection.cache([{ id: '1', a: 'a' }], { id: { $lt:'3' } }, {});

      const results = await collection.find({}, { sort: ['id'] });

      expect( results.map(result => result.id) ).toEqual(['1', '3']);
   });

   it('should remove missing sorted limited', async () => {
      await collection.cache([{ id: '1', a: 'a' }, { id: '2', a: 'b' }, { id: '3', a: 'c' }], {}, {});
      await collection.cache([{ id: '1', a: 'a' }], {}, { sort:[ 'id' ], limit: 2 } );

      const results = await collection.find({}, { sort: ['id'] });

      expect( results.map(result => result.id) ).toEqual(['1', '3']);
   });

   it('should not remove missing sorted limited past end', async () => {
      await collection.cache([{ id: '1', a: 'a' }, { id: '2', a: 'b' }, { id: '3', a: 'c' }, { id: '4', a: 'd' }], {}, {});
      await collection.remove('2');
      await collection.cache([{ id: '1', a: 'a' }, { id: '2', a: 'b' }], {}, { sort:['id'], limit: 2 });

      const results = await collection.find({}, { sort: ['id'] });

      expect( results.map(result => result.id) ).toEqual(['1', '3', '4']);
   });

   it('should not remove missing unsorted limited', async () => {
      await collection.cache([{ id: '1', a: 'a' }, { id: '2', a: 'b' }, { id: '3', a: 'c' }, { id: '4', a: 'd' }], {}, {});
      await collection.cache([{ id: '3', a: 'c' }, { id: '4', a: 'd' }], {}, { limit: 2 });

      const results = await collection.find({}, { sort: ['id'] });

      expect( results.map(result => result.id) ).toEqual(['1', '2', '3', '4']);
   });

   it('should remove matching when uncaching', async () => {
      await collection.cache([{ id: '1', a: 'a' }, { id: '2', a: 'b' }, { id: '3', a: 'c' }], {}, {});
      await collection.uncache({ a: 'b' });

      const results = await collection.find({}, { sort: ['id'] });

      expect( results.map(result => result.id) ).toEqual(['1', '3']);
   });

   it('should not remove upserts when uncaching', async () => {
      await collection.cache([{ id: '1', a: 'a' }, { id: '2', a: 'b' }, { id: '3', a: 'c' }], {}, {});
      await collection.upsert({ id: '2', a: 'b' });
      await collection.uncache({ a: 'b' });

      const results = await collection.find({}, { sort: ['id'] });

      expect( results.map(result => result.id) ).toEqual(['1', '2', '3']);
   });

   it('should not remove removed when uncaching', async () => {
      await collection.cache([{ id: '1', a: 'a' }, { id: '2', a: 'b' }, { id: '3', a: 'c' }], {}, {});
      await collection.remove('2');
      await collection.uncache({ a: 'b' });

      const results = await collection.find({}, { sort: ['id'] });

      expect( results.map(result => result.id) ).toEqual(['1', '3']);

      expect( await collection.pendingRemoves() ).toEqual(['2']);
   });

   it('should cache multiple elements', async () => {
      await collection.cacheList([{ id: '1', a: 'a' }, { id: '2', a: 'b' }, { id: '3', a: 'c' }]);

      const results = await collection.find({}, { sort: ['id'] });

      expect( results.map(result => result.id) ).toEqual(['1', '2', '3']);
   });

   it('should not overwrite upserted when caching multiple elements', async () => {
      await collection.upsert({ id: '1', a: 'apple' });
      await collection.cacheList([{ id: '1', a: 'banana' }]);
      
      const results = await collection.find({});

      expect( results[0].a ).toBe('apple');
   });

   it('should not overwrite remove when caching multiple elements', async () => {
      await collection.cacheList([{ id: '1', a: 'delete' }]);
      await collection.remove('1');
      await collection.cacheList([{ id: '1', a: 'banana' }]);

      const results = await collection.find({});

      expect( results ).toHaveLength(0);
   });

   it('should ids with uncacheList', async () => {
      await collection.cache([{ id: '1', a: 'a' }, { id: '2', a: 'b' }, { id: '3', a: 'c' }], {}, {});
      await collection.uncacheList(['2']);

      const results = await collection.find({});

      expect( results.map(result => result.id) ).toEqual(['1', '3']);
   });

   it('should remove upserts with uncacheList', async () => {
      await collection.cache([{ id: '1', a: 'a' }, { id: '2', a: 'b' }, { id: '3', a: 'c' }], {}, {});
      await collection.upsert({ id: '2', a: 'b' });
      await collection.uncacheList(['2']);

      const results = await collection.find({}, { sort: ['id'] });

      expect( results.map(result => result.id) ).toEqual(['1', '2', '3']);
   });

   it('should not remove removes with uncacheList', async () => {
      await collection.cache([{ id: '1', a: 'a' }, { id: '2', a: 'b' }, { id: '3', a: 'c' }], {}, {});
      await collection.remove('2');
      await collection.uncacheList(['2']);

      const results = await collection.find({}, { sort: ['id'] });

      expect( results.map(result => result.id) ).toEqual(['1', '3']);

      expect( await collection.pendingRemoves() ).toEqual(['2']);
   });

   it('should return pending upserts', async () => {
      await collection.cache([{ id: '1', a: 'apple' }], {}, {});
      await collection.upsert({ id: '2', a: 'banana' });

      const upserts = await collection.pendingUpserts();

      expect( upserts ).toHaveLength(1);
      expect( upserts[0].doc.a ).toBe('banana');
      expect( upserts[0].base ).toBe(null);
   });

   it('should resolve pending upserts', async () => {
      await collection.upsert({ id: '2', a: 'banana' });
      await collection.resolveUpserts([{ doc: { id: '2', a: 'banana' }, base: null }]);
      
      const upserts = await collection.pendingUpserts();

      expect( upserts ).toHaveLength(0);
   });

   it('should set base of upserts', async () => {
      await collection.cacheOne({ id: '2', a: 'apple' });
      await collection.upsert({ id: '2', a: 'banana' });

      const upserts = await collection.pendingUpserts();

      expect( upserts ).toHaveLength(1);
      expect( upserts[0].doc.a ).toBe('banana');
      expect( upserts[0].base.a ).toBe('apple');
   });

   it('should keep base on subsequent upserts', async () => {
      await collection.cacheOne({ id: '2', a: 'apple' });
      await collection.upsert({ id: '2', a: 'banana' });
      await collection.upsert({ id: '2', a: 'orange' });
      
      const upserts  = await collection.pendingUpserts();

      expect( upserts ).toHaveLength(1);
      expect( upserts[0].doc.a ).toBe('orange');
      expect( upserts[0].base.a ).toBe('apple');
   });

   it('should allow setting of upsert bases', async () => {
      await collection.upsert({ id: '2', a: 'banana' }, { id: '2', a: 'apple' });
      
      const upserts = await collection.pendingUpserts();

      expect( upserts ).toHaveLength(1);
      expect( upserts[0].doc.a ).toBe('banana');
      expect( upserts[0].base.a ).toBe('apple');
   });

   it('should allow setting of null upsert base', async () => {
      await collection.cacheOne({ id: '2', a: 'apple' })
      await collection.upsert({ id: '2', a: 'banana' }, null);
      
      const upserts = await collection.pendingUpserts();

      expect( upserts ).toHaveLength(1);
      expect( upserts[0].doc.a ).toBe('banana');
      expect( upserts[0].base ).toBe(null);
   });

   it('should allow multiple upserts', async () => {
      const docs = [
         { id: '1', a: 'apple' },
         { id: '2', a: 'banana' },
         { id: '3', a: 'orange' }
      ];

      await collection.upsert(docs);
      
      const upserts = await collection.pendingUpserts();

      expect( upserts.map(upsert => upsert.doc) ).toEqual( docs );
      expect( upserts.map(upsert => upsert.base) ).toEqual([ null, null, null ]);
   });

   it('should allow multiple upserts with bases', async () => {
      const docs = [
         { id: '1', a: 'apple' },
         { id: '2', a: 'banana' },
         { id: '3', a: 'orange' }
       ];
       const bases = [
         { id: '1', a: 'apple2' },
         { id: '2', a: 'banana2' },
         { id: '3', a: 'orange2' }
       ];
       await collection.upsert(docs, bases);

      const upserts = await collection.pendingUpserts();

      expect( upserts.map(upsert => upsert.doc) ).toEqual( docs );
      expect( upserts.map(upsert => upsert.base) ).toEqual( bases );
   });

   it('should resolve multiple upserts', async () => {
      const docs = [
         { id: '1', a: 'apple' },
         { id: '2', a: 'banana' },
         { id: '3', a: 'orange' }
       ];

       await collection.upsert( docs );

      const upserts = await collection.pendingUpserts();
      await collection.resolveUpserts( upserts );
      const upserts2 = await collection.pendingUpserts();

      expect( upserts2 ).toHaveLength(0);
   });

   it('should remove pending upserts', async () => {
      const docs = [
         { id: '1', a: 'apple' },
         { id: '2', a: 'bana;na' },
         { id: '3', a: 'orange' }
      ];
       
      await collection.upsert( docs );
      await collection.remove('1')
      await collection.resolveRemoves(['1']);
      
      const upserts = await collection.pendingUpserts();
      await collection.resolveUpserts( upserts );
      const upserts2 = await collection.pendingUpserts();

      expect( upserts2 ).toHaveLength(0);
   });

   it('should retain changed pending upserts but update base', async () => {
      await collection.upsert({ id: '2', a: 'banana' });
      await collection.upsert({ id: '2', a: 'banana2' });
      await collection.resolveUpserts([{ doc: { id: '2', a: 'banana' }, base: null }])
      
      const upserts = await collection.pendingUpserts();

      expect( upserts ).toHaveLength(1);
      expect( upserts[0].doc.a ).toBe('banana2');
      expect( upserts[0].base.a ).toBe('banana');
   });

   it('should remove by filter', async () => {
      await collection.upsert({ id: '1', a: 'apple' });
      await collection.upsert({ id: '2', a: 'banana' });
      await collection.upsert({ id: '3', a: 'banana' });
      await collection.remove({ a: 'banana' });
      
      const upserts = await collection.pendingUpserts();

      expect( upserts ).toHaveLength(1);
      expect( upserts[0].doc.a ).toBe('apple');
   });

   it('should remove pending upserts', async () => {
      await collection.upsert({ id: '2', a: 'banana' });
      await collection.remove('2');
      
      const upserts = await collection.pendingUpserts();

      expect( upserts ).toHaveLength(0);
   });
   
   it('should return pending removes', async () => {
      await collection.cache([{ id: '1', a: 'apple' }], {}, {});
      await collection.remove( '1');
      
      const removes = await collection.pendingRemoves();

      expect( removes ).toHaveLength(1);
      expect( removes ).toEqual(['1']);
   });

   it('should return pending removes that are not present', async () => {
      await collection.remove('2');

      const removes = await collection.pendingRemoves();

      expect( removes ).toHaveLength(1);
      expect( removes ).toEqual(['2']);
   });

   it('should resolve pending removes', async () => {
      await collection.cache([{ id: '1', a: 'apple' }], {}, {});
      await collection.remove('1');
      await collection.resolveRemoves(['1']);
      
      const removes = await collection.pendingRemoves();

      expect( removes ).toHaveLength(0);
   });

   it('should seed', async () => {
      await collection.seed([{ id: '1', a: 'apple' }]);
      
      const results = await collection.find({});

      expect( results[0].a ).toBe('apple');
   });

   it('should not overwrite existing data', async () => {
      await collection.cache([{ id: '1', a: 'banana' }], {}, {});
      await collection.seed([{ id: '1', a: 'apple' }]);

      const  results = await collection.find({});

      expect( results[0].a ).toBe('banana');
   });

   it('should not add removed', async () => {
      await collection.cache([{ id: '1', a: 'apple' }], {}, {});
      await collection.remove('1');
      await collection.seed([{ id: '1', a: 'apple' }]);
      
      const results = await collection.find({});

      expect( results ).toHaveLength(0);
   });

   it('should allow removing uncached rows', async () => {
      await collection.remove('12345');
      
      const removes = await collection.pendingRemoves();

      expect( removes ).toHaveLength(1);
      expect( removes ).toEqual(['12345']);
   });

   it('should seed rows', async () => {
      await collection.seed([{ id: '1', a: 'apple' }]);

      const results = await collection.find({});     

      expect( results[0].a ).toBe('apple');
   });

   it('should not overwrite data when seeding', async () => {
      await collection.cache([{ id: '1', a: 'apple' }], {}, {});
      await collection.seed({ id: '1', a: 'banana' });
      
      const results = await collection.find({});

      expect( results[0].a ).toBe('apple');
   });

   it('should not overwrite upserts when seeding', async () => {
      await collection.upsert({ id: '1', a: 'apple' });
      await collection.seed({ id: '1', a: 'banana' });
      
      const results = await collection.find({});

      expect( results[0].a ).toBe('apple');
   });

   it('should not overwrite removes when seeding', async () => {
      await collection.cache([{ id: '1', a: 'delete' }], {}, {});
      await collection.remove('1');
      await collection.seed({ id: '1', a: 'banana' });

      const results = await collection.find({});

      expect( results ).toHaveLength(0);
   });

   it('should cache one single doc', async () => {
      await collection.cacheOne({ id: '1', a: 'apple' });

      const results = await collection.find({});

      expect( results[0].a ).toBe('apple');
   });

   it('should overwrite existing when caching', async () => {
      await collection.cache([{ id: '1', a: 'apple' }], {}, {});
      await collection.cacheOne({ id: '1', a: 'banana' });

      const results = await collection.find({});

      expect( results[0].a ).toBe('banana');
   });

   it('should not overwrite upserts when caching', async () => {
      await collection.upsert({ id: '1', a: 'apple' });
      await collection.cacheOne({ id: '1', a: 'banana' });

      const results = await collection.find({});

      expect( results[0].a ).toBe('apple');
   });

   it('should not overwrite removes when caching', async () => {
      await collection.cache([{ id: '1', a: 'delete' }], {}, {});
      await collection.remove('1')
      await collection.cacheOne({ id: '1', a: 'banana' })

      const results = await collection.find({});

      expect( results ).toHaveLength(0);
   });

   it('should not overwrite existing with cache one when same._rev', async () => {
      await collection.cacheOne({ id: '1', a: 'apple', _rev: 2 });
      await collection.cacheOne({ id: '1', a: 'banana', _rev: 2 });

      const results = await collection.find({});

      expect( results[0].a ).toBe('apple');
   });

   it('should overwrite existing with cache one with greater _rev', async () => {
      await collection.cacheOne({ id: '1', a: 'apple', _rev: 1 });
      await collection.cacheOne({ id: '1', a: 'banana', _rev: 2 });

      const results = await collection.find({});

      expect( results[0].a ).toBe('banana');
   });

   it('should not overwrite existing with cache one when lesser _rev', async () => {
      await collection.cacheOne({ id: '1', a: 'apple', _rev: 2 });
      await collection.cacheOne({ id: '1', a: 'banana', _rev: 1 });

      const results = await collection.find({});

      expect( results[0].a ).toBe('apple');
   });

}