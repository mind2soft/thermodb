import HybridDb from '../src/HybridDb';
import MemoryDB from '../src/MemoryDB';

import standardQueries from './standard_queries';



describe('Testing HybridDb', () => {

   let local;
   let remote;
   let hybrid;

   let lc;
   let rc;
   let hc;

   const sleep = delay => new Promise(resolve => setTimeout(resolve, delay));
   
   const reset = async () => {
      local = new MemoryDB();
      remote = new MemoryDB();
      hybrid = new HybridDb(local, remote);

      await hybrid.addCollection('scratch');

      lc = local.scratch;
      rc = remote.scratch;
      hc = hybrid.scratch;
   };


   it('should create instance and collection', async () => {

      const local = new MemoryDB();
      const remote = new MemoryDB();

      const db = new HybridDb(local, remote);

      await db.addCollection('tests');

      expect( db.tests ).toBeTruthy();
      expect( db.localDb.tests ).toBeTruthy();
      expect( db.remoteDb.tests ).toBeTruthy();

      await db.removeCollection('tests');

      expect( db.tests ).toBeFalsy();
      expect( db.localDb.tests ).toBeFalsy();
      expect( db.remoteDb.tests ).toBeFalsy();
            
   });


   describe('Testing standard quries', () => {
      standardQueries(async () => {
         await reset();
         
         return hybrid.scratch;
      });
   });


   describe('interim:true (default)', () => {

      beforeEach(async () => reset());

      it('should only give one result if data unchanged', async () => {
         
         await lc.seed({ id: '1', a: 1 });
         await lc.seed({ id: '2', a: 2 });
 
         await rc.seed({ id: '1', a: 3 });
         await rc.seed({ id: '2', a: 4 });
 
         const results = await hc.find({});

         expect( results ).toHaveLength(2);
         expect( results.map(result => result.a) ).toEqual([1, 2]);
      });

      it('should call onRemoteData if remote has different values', async () => {
         
         await lc.seed({ id: '1', a: 1 });
         await lc.seed({ id: '2', a: 2 });
 
         await rc.seed({ id: '1', a: 3 });
         await rc.seed({ id: '2', a: 4 });
         
         return new Promise(async (resolve, reject) => {

            hc.options.onRemoteData = remoteResults => {
               try {
                  expect( remoteResults ).toHaveLength(2);
                  expect( remoteResults.map(result => result.a) ).toEqual([3, 4]);

                  resolve();
               } catch (error) {
                  reject(error);
               }
            };

            const localResults = await hc.find({});

            expect( localResults ).toHaveLength(2);
            expect( localResults.map(result => result.a) ).toEqual([1, 2]);
         });

      }, 200);

      it('should not call update if remote gives same answer with sort differences', async () => {
         await lc.seed({ id: '1', a: 1 });
         await lc.seed({ id: '2', a: 2 });
 
         rc.find = async () => [{ id: '2', a: 2}, { id: '1', a: 1 }];

         await hc.find({}, {
            onRemoteData: remoteDoc => {
               throw Error('Unexpected call');
            }
         });
      }, 200);

      it('should respect local upserts', async () => {
         await lc.seed({ id:'1', a:1 });
         await lc.upsert({ id:'2', a:2 });

         await rc.seed({ id:'1', a:1 });
         await rc.seed({ id:'2', a:4 });

         const doc = await hc.findOne({ id: '2' });

         expect( doc ).toEqual({ id: '2', a: 2 });
      });

   });


   describe('cacheFind: true (default)', () => {

      beforeEach(async () => reset());

      it('should perform full field remote queries with find', async () => new Promise(async (resolve, reject) => {
         try {
            await rc.seed({ id:'1', a:1, b:11 });
            await rc.seed({ id:'2', a:2, b:12 });

            const data = await hc.find({}, {
               fields: { b:0 },
               onRemoteData(remoteData) {
                  lc.findOne({ id: '1' }).then(localDoc => {

                     expect( remoteData ).toHaveLength(2);
                     expect( remoteData[0].b ).toBeFalsy();

                     expect( localDoc.b ).toBe(11);

                     resolve();
                  }).catch(reject);
               }
            });

            expect( data ).toHaveLength(0);
         } catch (error) {
            reject();
         }
      }), 200);

      it('should snapshot local upserts/removes to prevent race condition', async () => {
         // If the server receives the upsert/remove *after* the query and returns *before* the 
         // query does, a newly upserted item may be removed from cache
 
         lc.upsert({ id: '1', a: 1 });

         rc.find = async () => {
            // Simulate separate process having performed and resolved upsert
            const us = await lc.pendingUpserts();

            await lc.resolveUpserts(us);

            return [];
         };

         const data = await hc.find({}, { interim: false });

         expect( data ).toHaveLength(1);
      });

   });


   describe('cacheFindOne: true (default)', () => {
      
      beforeEach(async () => reset());

      it('should perform full field remote query with findOne', async () => {
         await rc.seed({ id: '1', a: 1, b: 11 });
         await rc.seed({ id: '2', a: 2, b: 12 });
 
         const doc = await hc.findOne({ id: '1' }, { fields: { b: 0 } });

         expect( doc.b ).toBeUndefined();

         const doc2 = await hc.findOne({ id: '1' });

         expect( doc2.b ).toBe(11);
      });

      it('should return local result as well as call onRemoteData if different result on remote', async () => {

         await lc.seed({ id: '1', a: 1 });
         await lc.seed({ id: '2', a: 2 });
 
         await rc.seed({ id: '1', a: 3 });
         await rc.seed({ id: '2', a: 4 });

         const [ localDoc, remoteDoc ] = await new Promise((resolve, reject) => {
            let hasLocal = false, localDoc;
            let hasRemote = false, remoteDoc;

            hc.findOne({ id: '1' }, {
               onRemoteData: remoteData => {
                  if (remoteData && remoteData.length) {
                     remoteDoc = remoteData[0];
                  }

                  if (hasLocal) {
                     resolve([ localDoc, remoteDoc ]);
                  } else {
                     hasRemote = true;
                  }
               }
            }).then(doc => {
               localDoc = doc;

               if (hasRemote) {
                  resolve([ localDoc, remoteDoc ]);
               } else {
                  hasLocal = true;
               }
            }, reject);
         });

         expect( localDoc.a ).toBe(1);
         expect( remoteDoc.a ).toBe(3);

      }, 200);

      it('should not fail and return local even if remote fails', async () => {
         let errorThrown = false;

         await lc.seed({ id: '1', a: 1 });

         rc.findOne = async () => { throw Error('fail'); };
         rc.find = async () => { throw Error('fail'); };

         const doc = await new Promise(resolve => {
            let hasLocal = false, localDoc;
            let hasError = false;
                        
            hc.findOne({ id: '1' }, { 
               debug: true,
               onRemoteError: () => {
                  errorThrown = true;

                  if (hasLocal) {
                     resolve(localDoc);
                  } else {
                     hasError = true;
                  }
               }
            }).then(doc => {
               localDoc = doc;

               if (hasError) {
                  resolve(localDoc);
               } else {               
                  hasLocal = true;
               }
            })
         });

         expect( doc.a ).toBe(1);
         expect( errorThrown ).toBeTruthy();
      }, 200);

      it('should return selected not by id even if remote fails', async () => {
         await lc.seed({ id: '1', a: 1 });

         rc.findOne = async () => { throw Error('fail'); };
         rc.find = async () => { throw Error('fail');};
 
         const doc = await new Promise(resolve => {
            let hasLocal = false, localDoc;
            let hasError = false;

            hc.findOne({ a: 1 }, {
               onRemoteError: () => {
                  if (hasLocal) {
                     resolve(localDoc);
                  } else {
                     hasError = true;
                  }
               }
            }).then(doc => {
               if (hasError) {
                  resolve(doc);
               } else {
                  hasLocal = true;
                  localDoc = doc;
               }
            });
         });
         
         expect( doc.a ).toBe(1);
      });

      it('should keep local cache updated when remote change', async () => {
         await lc.seed({ id: '1', a: 1 });
         await lc.seed({ id: '2', a: 2 });
 
         await rc.seed({ id: '1', a: 3 });
         await rc.seed({ id: '2', a: 4 });
 
         const [ localDoc, remoteDoc ] = await new Promise(resolve => {
            let hasLocal = false, localDoc;
            let hasRemote = false, remoteDoc;

            hc.findOne({ id: '1' }, {
               onRemoteData: remoteData => {
                  remoteDoc = remoteData.length ? remoteData[0] : null;

                  if (hasLocal) {
                     resolve([ localDoc, remoteDoc ]);
                  } else {
                     hasRemote = true;                     
                  }
               }
            }).then(doc => {
               localDoc = doc;

               if (hasRemote) {
                  rsolve([ localDoc, remoteDoc ]);
               } else {
                  hasLocal = true;
               }
            });
         });

         const localResults = await lc.find({});

         expect( localDoc.a ).toBe(1);
         expect( remoteDoc.a ).toBe(3);
         expect( localResults.map(result => result.a) ).toEqual([3, 2]);
      });
      
   });
   

   describe('interim: false', () => {
      
      beforeEach(async () => reset());

      it('should return final result only with find', async () => {
         await lc.upsert({ id:'1', a: 1 });
         await lc.seed({ id:'2', a: 2 });
 
         await rc.seed({ id:'1', a: 3 });
         await rc.seed({ id:'2', a: 4 });
 
         const results = await hc.find({}, { interim: false });

         expect( results ).toHaveLength(2);
         expect( results.map(result => result.a) ).toEqual([1, 4]);
      });

      it('should findOne from both local and remote', async () => {
         await lc.seed({ id: '1', a: 1 });
         await lc.seed({ id: '2', a: 2 });

         await rc.seed({ id: '1', a: 3 });
         await rc.seed({ id: '2', a: 4 });

         const doc = await hc.findOne({ id: '1' }, { interim: false });

         expect( doc.a ).toBe(3);
      });

   });


   describe('interim: false with timeout', () => {

      beforeEach(async () => reset());

      it('should give final result if in time', async () => {
         await lc.upsert({ id:'1', a: 1 });
         await lc.seed({ id:'2', a: 2 });
  
         rc.find = async () => {
            await sleep(100);
            return [{ id: '1', a: 3 }, { id: '2', a: 4 }];
         };

         const results = await hc.find({}, { interim: false, timeout: 200 });

         expect( results ).toHaveLength(2);
         expect( results.map(result => result.a) ).toEqual([1, 4]);
      }, 500);

      it('should give local result if out of time', async () => {
         await lc.upsert({ id: '1', a: 1 });
         await lc.seed({ id: '2', a: 2 });
 
         rc.find = async () => {
            // Wait a bit too long
            await sleep(300);
            return [{ id: '1', a: 3 }, { id: '2', a: 4 }];
         };
 
         const results = await hc.find({}, { interim: false, timeout: 200 });

         expect( results ).toHaveLength(2);
         expect( results.map(result => result.a) ).toEqual([1, 2]);
      }, 500);

      it('should return local results but still cache if out of time', async () => {
         await lc.upsert({ id: '1', a: 1 });
         await lc.seed({ id: '2', a: 2 });
 
         rc.find = async () => {
            await sleep(300);
            
            return [{ id: '1', a: 3  }, { id: '2', a: 4 }];
         };
 
         const results = await hc.find({}, { interim: false, timeout: 200 });

         expect( results ).toHaveLength(2);
         expect( results.map(result => result.a) ).toEqual([1, 2]);
 
         // Wait longer for remote to complete
         await sleep(200);
         
         const localResults = await lc.find({}, {});

         expect( localResults ).toHaveLength(2);
         // note: id:'1' is an upsert, so it should not change on local
         expect( localResults.map(result => result.a) ).toEqual([1, 4]);
      }, 500);

      it('should return local results if remote time out and fails', async () => {
         await lc.upsert({ id: '1', a: 1 });
         await lc.seed({ id: '2', a: 2 });
 
         rc.find = async () => {
            await sleep(300);

            throw Error('fail');
         };

         const results = await hc.find({}, { interim: false, timeout: 200 });

         expect( results ).toHaveLength(2);
         expect( results.map(result => result.a) ).toEqual([1, 2]);
      });

   });


   describe('cacheFind: false', () => {
      
      beforeEach(async () => reset());

      it('should perform partial field remote queries', async () => {
         await rc.seed({ id: '1', a: 1, b: 11 });
         await rc.seed({ id: '2', a: 2, b: 12 });

         const oldFind = rc.find;
         rc.find = (...args) => {
            expect( args[1].fields ).toEqual({ b: 0 });

            return oldFind.call(rc, ...args);
         };

         const [ localData, remoteData ] = await new Promise(resolve => {
            let hasLocal = false, localData;
            let hasRemote = false, remoteData;

            hc.find({}, {
               fields: { b: 0 },
               cacheFind: false,
               onRemoteData: data => {
                  if (hasLocal) {
                     resolve([ localData, data ]);
                  } else {
                     hasRemote = true;
                     remoteData = data;
                  }
               }
            }).then(data => {
               if (hasRemote) {
                  resolve([ data, remoteData ]);
               } else {
                  hasLocal = true;
                  localData = data;
               }
            });
         });

         expect( localData ).toHaveLength(0);
         expect( remoteData ).toHaveLength(2);
         expect( remoteData[0].b ).toBeUndefined();
      });

      it('should not cache remote data', async () => {
         await lc.seed({ id: '1', a: 1 });
         await lc.seed({ id: '2', a: 2 });
 
         await rc.seed({ id: '1', a: 3 });
         await rc.seed({ id: '2', a: 2 });
          
         const results = await hc.find({}, { cacheFind: false });

         expect( results ).toHaveLength(2);

         await sleep(100);

         // After some delay, check that local collection is unchanged
         const localResults = await lc.find({});

         expect( localResults.map(result => result.a ) ).toEqual([1, 2]);
      });

   });


   describe('cacheFindOne: false', () => {

      beforeEach(async () => reset());

      it('should perform partial field remote queries', async () => {
         await rc.seed({ id: '1', a: 1, b: 11 });
         await rc.seed({ id: '2', a: 2, b: 12 });

         const oldFind = rc.find;
         rc.find = (...args) => {
            expect( args[1].fields ).toEqual({ b: 0 });

            return oldFind.call(rc, ...args);
         };

         const doc = await hc.findOne({ id: '1' }, { fields: { b: 0 }, cacheFindOne: false });

         expect( doc.b ).toBeUndefined();
      });

   });


   describe('shortcut: false (default', () => {
      
      beforeEach(async () => reset());

      it('should call both local and remote', async () => {
         await lc.seed({ id:'1', a:1 });
         await lc.seed({ id:'2', a:2 });
 
         await rc.seed({ id:'1', a:3 });
         await rc.seed({ id:'2', a:4 });
 
         const [ localDoc, remoteDoc ] = await new Promise(resolve => {
            let hasLocal = false, localDoc;
            let hasRemote = false, remoteDoc;

            hc.findOne({ id: '1' }, {
               onRemoteData: remoteData => {
                  remoteDoc = remoteData.length ? remoteData[0] : null;

                  if (hasLocal) {
                     resolve([ localDoc, remoteDoc ]);
                  } else {
                     hasRemote = true;                     
                  }
               }
            }).then(doc => {
               localDoc = doc;

               if (hasRemote) {
                  rsolve([ localDoc, remoteDoc ]);
               } else {
                  hasLocal = true;
               }
            });
         });

         expect( localDoc.a ).toBe(1);
         expect( remoteDoc.a ).toBe(3);
      });

   });


   describe('shortcut: true', () => {

      beforeEach(async () => reset());

      it('should call remote if local not found', async () => {
         await lc.seed({ id: '2', a: 2 });

         await rc.seed({ id: '1', a: 3 });
         await rc.seed({ id: '2', a: 4 });
 
         const doc = await hc.findOne({ id: '1' }, { shortcut: true });

         expect( doc.a ).toBe(3);
      });

      it('should only call local if found', async () => {
         await lc.seed({ id:'1', a: 1 })
         await lc.seed({ id:'2', a: 2 })
 
         await rc.seed({ id:'1', a: 3 });
         await rc.seed({ id:'2', a: 4 });
 
         rc.find = (...args) => {
            throw Error('fail');
         };

         const doc = await hc.findOne({ id: '1' }, { shortcut: true })

         expect( doc.a ).toBe(1);

         await sleep(50);
      }, 100);

   });


   describe('synchronizing remote and local', () => {
      
      beforeEach(async () => reset());
      
      it('should applies pending upserts when upload is called', async () => {
         await lc.upsert({ id: '1', a: 1 });
         await lc.upsert({ id: '2', a: 2 });
   
         await hc.upload();

         const localUpserts = await lc.pendingUpserts();
         
         expect( localUpserts ).toHaveLength(0);

         const remoteUpserts = await rc.pendingUpserts();

         expect( remoteUpserts.map(u => u.doc.a) ).toEqual([1, 2]);
      });
      
   });


   describe('updating uploads', () => {

      beforeEach(async () => reset());

      it('should update upserts if different id', async () => {

         const oldUpsert = rc.upsert;
         rc.upsert = async docs => {
            docs = JSON.parse(JSON.stringify(docs));
            docs.id = "123";

            return oldUpsert.call(rc, docs);
         };

         const localUpsert = await hc.upsert({ a: 9 });
         
         const changes = await hc.upload();
         
         const results = await hc.find({});

         expect( localUpsert.id ).not.toBe('123');
         expect( results ).toHaveLength(1);
         expect( results[0].id ).toBe('123');
         expect( changes ).toEqual({ doc: [{ id: results[0].id }], base: [{ id: localUpsert.id }] });

      });

   });

});