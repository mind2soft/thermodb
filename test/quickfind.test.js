import sha1 from 'js-sha1';

import { encodeRequest, encodeResponse, decodeResponse } from '../src/quickfind';



describe('Testing quickfind', () => {

   const KEY = 'id';


   const completeTest = (clientRows, serverRows, sort) => {
      const encodedRequest = encodeRequest(clientRows, KEY)
      const encodedResponse = encodeResponse(serverRows, encodedRequest, KEY)
      const decodedResponse = decodeResponse(encodedResponse, clientRows, sort, KEY)

      //assert.deepEqual decodedResponse, serverRows, JSON.stringify(decodedResponse, null, 2) + ' vs ' + JSON.stringify(serverRows, null, 2)

      expect( decodedResponse ).toEqual( serverRows );
   };

   const row1 = { id: '0000', _rev: 1, a: 2 };
   const row2 = { id: '0001', _rev: 2, a: 1 };
   const row2a = { id: '0001', _rev: 3, a: 3 };
   const row3 = { id: '0100', _rev: 1, a: 3 };


   it('should encode as expected', () => {

      const request = encodeRequest([ row3, row2, row1 ], KEY);

      expect( Object.keys(request) ).toHaveLength(2);
      expect( request['00'] ).toBe( sha1("0000:1|0001:2|").substr(0, 20) );
      expect( request['01'] ).toBe( sha1("0100:1|").substr(0, 20) );
   });

   it('should only include changes', () => {
      const request = encodeRequest([row3, row2, row1], KEY)
      const response = encodeResponse([row3, row1], request, KEY);

      expect( Object.keys( response ) ).toHaveLength(1);
      expect( response['00'] ).toEqual([row1]);
   });

   it('should start with blank', () => completeTest([], [row1, row2, row3]));

   it('should go to blank', () => completeTest([row1, row2, row3], []));

   it('should replace', () => completeTest([row1, row2, row3], [row1, row2a, row3]));

   it('should sort', () => completeTest([row2, row1, row3], [row2, row1, row3], ['a']));

});