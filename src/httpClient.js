import AbortController from "abort-controller";



// return true if th request support POST body
const isPost = method => method && 'POST,PATCH'.indexOf(String(method).toLocaleUpperCase()) >= 0;



// return URL-safe string from object
const serializeParams = params => {
   const str = [];
   for (let p in params) {
      if (params.hasOwnProperty(p)) {
         let param = params[p];

         if (typeof param === 'object') {
            param = JSON.stringify(param);
         }

         str.push(encodeURIComponent(p) + "=" + encodeURIComponent(param));
      }
   }
   return str.join("&");
};


// synchronously return a Promise
const httpClient = (method, url, params, body, options) => {
   let timer = null;

   options = options || {};
   options.method = method || 'GET';

   if (isPost(method)) {
      options.body = JSON.stringify(body || {});
   } else if (body) {
      Object.assign(params, body);
   }

   if (params && Object.keys(params).length) {
      url = url + (url.indexOf('?') >= 0 ? '&' : '?') + serializeParams(params);
   }

   if (options.timeout) {
      const controller = new AbortController();
      const timeout = options.timeout;

      delete options.timeout;

      options.signal = controller.signal;

      timer = setTimeout(() => controller.abort(), timeout);
   }

   return fetch(url, options).then(response => {
      if (timer) {
         clearTimeout(timer);
      }
      
      if (response.status === 200) {
         return response.json();
      } else if (response.status === 410) {
         return null;
      } else {
         throw Error(response.statusText);
      }
   });
};



export default httpClient;
export { serializeParams }