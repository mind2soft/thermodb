import IDBStore from 'idb-wrapper';

import EJSON from './EJSON';
import {
   KeyUtil,
   normalizeUpsert,
   processFind
} from "./utils";
import {
   compileSort,
   compileDocumentSelector
} from './selector';



class IndexedDb {

   ready = false;
   error = null;

   store = null;
   collections = {};
   options = { namespace: 'default', keyUtil: null };

   constructor(options) {
      const { onReady, onError, ...otherOptions } = options || {};
      
      Object.assign(this.options, otherOptions || {});

      if (!this.options.keyUtil) {
         this.options.keyUtil = new KeyUtil.Default();
      }
      if (!this.options.key) {
         this.options.key = this.options.keyUtil.getField();
      }

      this.store = new IDBStore({
         dbVersion: 1,
         storeName: 'thermodb_' + this.options.namespace,
         keyPath: ['col', `doc.${this.options.key}`],
         autoIncrement: false,
         onStoreReady: () => {
            this.ready = true;
            onReady && onReady(this);
         },
         onError: error => {
            this.error = error;
            onError && onError(error);
         },
         indexes: [
           { name: 'col', keyPath: 'col', unique: false, multiEntry: false },
           { name: 'col-state', keyPath: ['col', 'state'], unique: false, multiEntry: false}
         ]
      });
   }

   async addCollection(name, options) {
      if (name === 'collections' || name === 'store' || name === 'options') {
         throw Error('Illegal operation on IndexedDb instance');
      }

      if (!this.ready) {
         await new Promise((resolve, reject) => {
            const timeout = Date.now() * 1000 * 10; // 10 secs.
            const next = () => {
               if (this.ready) resolve();
               if (this.error) reject(this.error);
               if (timeout < Date.now()) {
                  reject(Error('Store timeout'));
               } else {
                  setTimeout(next, 0);
               }
            };

            next();
         });
      }

      options = options || {};

      const collection = new IndexedDbCollection(name, this.store, { ...this.options, ...options });
      this[name] = collection;
      this.collections[name] = collection;
      return collection;
   }

   async removeCollection(name) {
      if (name === 'collections' || name === 'store' || name === 'options') {
         throw Error('Illegal operation on IndexedDb instance');
      }

      const key = this.options.keyUtil.getField();

      delete this[name];
      delete this.collections[name];

      return new Promise((resolve, reject) => {
         this.store.query(matches => {
            const keys = matches.map(m => [m.col, m.doc[key]]);

            if (keys.length) {
               this.store.removeBatch(keys, resolve, reject);
            } else {
               resolve();
            }
         }, { index: "col", keyRange: this.store.makeKeyRange({ only: name }), onError: reject });
      });
   }

   getCollectionNames() {
      return Object.keys(this.collections);
   }

}





class IndexedDbCollection {

   constructor(name, store, options) {
      if (!options || !options.keyUtil) {
         throw Error('Missing keyUtil');
      }

      this.name = name;
      this.store = store;
      this.options = options;

      if (!this.options.key) {
         this.options.key = this.options.keyUtil.getField();
      }
   }

   async find(selector, options) {
      const key = this.options.key;

      return new Promise((resolve, reject) => {
         this.store.query(matches => {
            try {
               const items = matches.filter(m => m.state !== 'removed').map(m => m.doc);

               resolve( processFind(items, selector, options, key) );
            } catch (error) {
               reject(error);
            }
         }, { index: "col", keyRange: this.store.makeKeyRange({ only: this.name }), onError: reject });
      });
   }

   async findOne(selector, options) {
      const items = await this.find(selector, options);

      return items && items.length ? items[0] : null;
   }

   async upsert(docs, bases) {
      const key = this.options.key;
      const items = JSON.parse(JSON.stringify( normalizeUpsert(docs, bases, key, this.options.keyUtil) ));
      const docKeys = items.map(item => ([ this.name, item.doc[key] ]));

      return new Promise((resolve, reject) => {
         this.store.getBatch(docKeys, records => {
            const puts = items.map((item, i) => {
               let base;

               if (item.base !== undefined) {
                  base = item.base;
               } else if (records[i] && records[i].doc && records[i].state === 'cached') {
                  base = records[i].doc;
               } else if (records[i] && records[i].doc && records[i].state === 'upserted') {
                  base = records[i].base;
               } else {
                  base = null;
               }

               return {
                  col: this.name,
                  state: 'upserted',
                  doc: item.doc,
                  base
               };
            });

            this.store.putBatch(puts, success => {
               if (success) {
                  resolve(docs);
               } else {
                  reject(Error('Failed to insert document'));
               }
            }, reject);
         }, reject);
      });
   }

   async remove(docKey) {
      const key = this.options.key;

      if (this.options.keyUtil.isKey(docKey)) {
         return new Promise((resolve, reject) => {
            this.store.get([ this.name, docKey ], record => {
               record = record  || { col: this.name, doc: { [key]: docKey }};
               
               record.state = 'removed';

               this.store.put(record, resolve, reject);
            });
         });
      } else {
         const docs = await this.find(docKey);

         for (const doc of docs) {
            await this.remove(doc[key]);
         }

         return docs.length;
      }
   }

   async cache(docs, selector, options) {
      const key = this.options.key;

      options = options || {};

      const step2 = async () => {
         // Rows have been cached, now look for stale ones to remove
         const docsSet = new Set(docs.map(doc => doc[key]));

         const sort = options.sort ? compileSort(options.sort) : null;

         // Perform query, removing rows missing in docs from local db
         const results = await this.find(selector, options);

         const keys = results.map(result => ([this.name, result[key]]));

         if (!keys.length) {
            return;
         }

         const lastDoc = docs[docs.length - 1];

         return new Promise((resolve, reject) => {
            const removes = [];

            this.store.getBatch(keys, records => {
               for (let i = 0, len = results.length; i < len; ++i) {
                  const record = records[i];
                  const result = results[i];

                  // If not present in docs and is present locally and not upserted/deleted
                  if (!docsSet.has(result[key]) && record && record.state === 'cached') {
                     // If at limit
                     if (options.limit && (docs.length === options.limit)) {
                        // If past end on sorted limited, ignore
                        if (!sort || sort(result, lastDoc) >= 0) {
                           continue;
                        }
                     }

                     // Exclude any excluded _ids from being cached/uncached
                     if (options.exclude && options.exclude.includes(result[key])) {
                        continue;
                     }

                     // Item is gone from server, remove locally
                     removes.push([ this.name, result[key] ]);
                  }
               }

               if (removes.length) {
                  this.store.removeBatch(removes, resolve, reject);
               } else {
                  resolve();
               }
            }, reject);
         });
      };


      if (!docs.length) {
         return step2();
      } else {
         return new Promise((resolve, reject) => {
            // Create keys to get items
            const keys = docs.map(doc => ([ this.name, doc[key] ]));

            // Create batch of puts
            const puts = [];

            this.store.getBatch(keys, records => {
               // Add all non-local that are not upserted or removed
               for (let i = 0, len = records.length; i < len; ++i) {
                  const record = records[i];
                  const doc = docs[i];
                  
                  // Check if not present or not upserted/deleted
                  if (!record || record.state === 'cached') {
                     if (options.exclude && options.exclude.includes(doc[key])) {
                        continue;
                     }

                     // If _rev present, make sure that not overwritten by lower or equal _rev
                     if (!record || !doc._rev || !record.doc._rev || doc._rev > record.doc._rev) {
                        puts.push({ col: this.name, state: 'cached', doc });
                     }
                  }
               }

               if (puts.length) {
                  this.store.putBatch(puts, () => step2().then(resolve, reject), reject);
               } else {
                  step2().then(resolve, reject);
               }
            }, reject);
         });
      }
   }

   async pendingUpserts() {
      return new Promise((resolve, reject) => {
         this.store.query(matches => {
            resolve( matches.map(m => ({ doc: m.doc, base: m.base || null })) );
         }, { index: "col-state", keyRange: this.store.makeKeyRange({ only: [ this.name, "upserted"] }), onError: reject });
      });
   }

   async pendingRemoves() {
      const key = this.options.key;
      
      return new Promise((resolve, reject) => {
         this.store.query(matches => {
            resolve( matches.map(m => m.doc[key]) );
         }, { index: "col-state", keyRange: this.store.makeKeyRange({ only: [ this.name, "removed"] }), onError: reject });
      });
   }

   async resolveUpserts(upserts) {
      const key = this.options.key;
      // Get items
      const keys = upserts.map(upsert => [ this.name, upsert.doc[key] ]);

      return new Promise((resolve, reject) => {
         this.store.getBatch(keys, records => {
            const puts = [];

            for (let i = 0, len = records.length; i < len; ++i) {
               const record = records[i];

               // Only safely remove upsert if doc is the same
               if (record && record.state === 'upserted') {
                  if (EJSON.equals(record.doc, upserts[i].doc)) {
                     record.state = 'cached';
                     puts.push(record);
                  } else {
                     record.base = upserts[i].doc;
                     puts.push(record);
                  }
               }
            }

            // Put all changed items
            if (puts.length) {
               this.store.putBatch(puts, resolve, reject);
            } else {
               resolve();
            }
         }, reject);
      });
   }

   async resolveRemoves(removes) {
      if (!removes || !removes.length) {
         return;
      }

      const key = this.options.key;

      return new Promise((resolve, reject) => {
         const keys = removes.map(remove => ([ this.name, remove ]));

         this.store.getBatch(keys, records => {
            const removeKeys = records.filter(record => record && record.state === 'removed').map(record => ([ this.name, record.doc[key] ]));

            this.store.removeBatch(removeKeys, resolve, reject);
         }, reject);
      });
   }

   // Add but do not overwrite or record as upsert
   async seed(docs) {
      if (!Array.isArray(docs)) {
         docs = [docs];
      }
      
      const key = this.options.key;
      // Create keys to get items
      const keys = docs.map(doc => ([ this.name, doc[key] ]));

      return new Promise((resolve, reject) => {
         // create batch of puts
         const puts = [];
         this.store.getBatch(keys, records => {
            // Add all non-local that are not upserted or removed
            for (let i = 0, len = records.length; i < len; ++i) {
               const record = records[i];
               const doc = docs[i];

               if (!record) {
                  puts.push({ col: this.name, state: 'cached', doc });
               }
            }

            if (puts.length) {
               this.store.putBatch(puts, resolve, reject);
            } else {
               resolve();
            }
         }, reject);
      });
   }

   // Add but do not overwrite upsert/removed and do not record as upsert
   async cacheOne(doc) {
      return this.cacheList([doc]);
   }

   async cacheList(docs) {
      const key = this.options.key;
      // Create keys to get items
      const docKeys = docs.map(doc => ([ this.name, doc[key] ]));
      
      return new Promise((resolve, reject) => {
         // Create batch of puts
         const puts = [];

         this.store.getBatch(docKeys, records => {
            for (let i = 0, len = records.length; i < len; ++i) {
               const doc = docs[i];
               let record = records[i];
      
               // If _rev present, make sure that not overwritten by lower equal _rev
               if (record && doc._rev && record.doc._rev && doc._rev <= record.doc._rev) {
                  continue;
               }
               
               if (!record) {
                  record = {
                     col: this.name,
                     state: 'cached',
                     doc
                  };
               }

               if (record.state === 'cached') {
                  record.doc = doc;
                  puts.push(record);
               }
            }

            if (puts.length) {
               this.store.putBatch(puts, resolve, reject);
            } else {
               resolve();
            }
         }, reject);
      });  
   }

   async uncache(selector) {
      const key = this.options.key;
      const compiledSelector = compileDocumentSelector(selector);

      return new Promise((resolve, reject) => {
         // Get all docs from collection
         this.store.query(matches => {
            // Filter ones to remove
            matches = matches.filter(m => m.state === 'cached' && compiledSelector(m.doc));

            const keys = matches.map(m => ([ this.name, m.doc[key] ]));

            if (keys.length) {
               this.store.removeBatch(keys, resolve, reject);
            } else {
               resolve();
            }
         }, { index: "col", keyRange: this.store.makeKeyRange({ only: this.name }), onError: reject });
      });
   }

   async uncacheList(docKeys) {
      const key = this.options.key;
      const idIndex = new Set(docKeys);

      return new Promise((resolve, reject) => {
         this.store.query(matches => {
            // Filter ones to remove
            matches = matches.filter(m => m.state === 'cached' && idIndex.has(m.doc[key]));

            const keys = matches.map(m => ([ this.name, m.doc[key] ]));

            if (keys.length) {
               this.store.removeBatch(keys, resolve, reject);
            } else {
               resolve()
            }
         }, { index: "col", keyRange: this.store.makeKeyRange({ only: this.name }), onError: reject });
      });
   }

}



export default IndexedDb;
export { IndexedDbCollection };