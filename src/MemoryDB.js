import { 
   compileSort,
   compileDocumentSelector
} from './selector';

import EJSON from './EJSON';

import { 
   KeyUtil,
   normalizeUpsert,
   processFind
} from './utils';



class MemoryDb {

   collections = {};
   options = { keyUtil: null, safety: "clone" };

   constructor(options) {
      Object.assign(this.options, options || {});

      if (!this.options.keyUtil) {
         this.options.keyUtil = new KeyUtil.Default();
      }
   }

   async addCollection(name, options) {
      if (name === 'collections' || name === 'options') {
         throw Error('Illegal operation on MemoryDb instance');
      }

      options = options || {};

      const collection = new MemoryCollection(name, { ...this.options, ...options });
      this[name] = collection;
      this.collections[name] = collection;
      return collection;
   }

   async removeCollection(name) {
      if (name === 'collections' || name === 'options') {
         throw Error('Illegal operation on MemoryDb instance');
      }

      delete this[name];
      delete this.collections[name];
   }

   getCollectionNames() {
      return Object.keys(this.collections);
   }

}


class MemoryCollection {

   constructor(name, options) {
      if (!options || !options.keyUtil) {
         throw Error('Missing keyUtil');
      }

      this.name = name;
      this.items = {};
      this.upserts = {};
      this.removes = {};
      this.options = options;

      if (!this.options.key) {
         this.options.key = this.options.keyUtil.getField();
      }
   }

   _applySafety(items) {
      if (!items) {
         return items;
      } else if (Array.isArray(items)) {
         return items.map(this._applySafety, this);
      } else if (!this.options.safety || (this.options.safety === 'clone')) {
         return JSON.parse(JSON.stringify(items));
      } else if (this.options.safety === 'freeze') {
         return Object.freeze(items);
      } else {
         throw new Error(`Unsupported safety option ${this.options.safety}`);
      }
   }

   async find(selector, options) {
      const key = this.options.key;
      let items;

      if (selector && selector[key] && this.options.keyUtil.isKey(selector[key])) {
         const keyValue = String(selector[key]);

         items = this.items[keyValue] ? [this.items[keyValue]] : [];
      } else {
         items = Object.keys(this.items).map(keyValue => this.items[keyValue]);
      }

      return this._applySafety( processFind(items, selector, options, key) );
   }

   async findOne(selector, options) {
      const items = await this.find(selector, options);

      return items.length ? items[0] : null
   }


   async upsert(docs, bases) {
      const key = this.options.key;
      const items = JSON.parse(JSON.stringify( normalizeUpsert(docs, bases, key, this.options.keyUtil) ));

      for (let item of items) {
         const docKey = item.doc[key];

         if (item.base === undefined) {
            if (this.upserts[docKey]) {
               item.base = this.upserts[docKey].base;
            } else {
               item.base = this.items[docKey] || null;
            }
         }

         this.items[docKey] = item.doc;
         this.upserts[docKey] = item;
      }

      return this._applySafety( Array.isArray(docs) ? items.map(item => item.doc) : items[0].doc );
   }

   async remove(docKey) {
      const key = this.options.key;
      let count = 0;

      if (docKey && !this.options.keyUtil.isKey(docKey)) {
         const docs = await this.find(docKey);

         for (const doc of docs) {
            await this.remove(doc[key]);
         }

         count = docs.length;
      } else if (this.items[docKey]) {
         this.removes[docKey] = this.items[docKey];
         delete this.items[docKey];
         delete this.upserts[docKey];

         count = 1;
      } else if (docKey) {
         this.removes[docKey] = { [key]: docKey };
      }

      return count;
   }

   async cache(docs, selector, options) {
      const key = this.options.key;
      options = options || {};

      for (const doc of docs) {
         if (!options.exclude || !options.exclude.includes( doc[key] )) {
            await this.cacheOne(doc)
         }
      }

      const docsSet = new Set(docs.map(doc => doc[key]));

      const sort = options.sort ? compileSort(options.sort) : null;

      const results = await this.find(selector, options);
      const lastDoc = docs[docs.length - 1];

      for (const result of results) {
         if (!docsSet.has(result[key]) && !this.upserts[result[key]]) {
            // if limit
            if (options.limit && docs.length === options.limit) {
               // If past end on sorted limited, ignore
               if (!sort || (sort(result, lastDoc) >= 0)) {
                  continue;
               }
            }
            if (options.exclude && options.exclude.includes( result[key] )) {
               continue;
            }

            delete this.items[result[key]];
         }
      }
   }

   async pendingUpserts() {
      return Object.keys(this.upserts).map(key => this.upserts[key]);
   }

   async pendingRemoves() {
      return Object.keys(this.removes);
   }

   async resolveUpserts(upserts) {
      const key = this.options.key;

      for (const upsert of upserts) {
         const docKey = upsert.doc[key];

         if (this.upserts[docKey]) {
            if (EJSON.equals(upsert.doc, this.upserts[docKey].doc)) {
               delete this.upserts[docKey];
            } else {
               this.upserts[docKey].base = upsert.doc;
            }
         }
      }
   }

   async resolveRemoves(removes) {
      for (const docKey of removes) {
         delete this.removes[docKey];
      }
   }


   async seed(docs) {
      const key = this.options.key;

      docs = Array.isArray(docs) ? docs : [docs];

      for (const doc of docs) {
         if (!this.items[doc[key]] && !this.removes[doc[key]]) {
            this.items[doc[key]] = doc;
         }
      }
   }


   // Add but do not overwrite upserts or removes
   async cacheOne(doc) {
     return this.cacheList([doc]);
   }
 
   // Add but do not overwrite upserts or removes
   async cacheList(docs) {
      const key = this.options.key;

      for (const doc of docs) {
         const docKey = doc[key];

         if (!this.upserts[docKey] && !this.removes[docKey]) {
            const existing = this.items[docKey];
 
            // If _rev present, make sure that not overwritten by lower or equal _rev
            if (!existing || !doc._rev || !existing._rev || (doc._rev > existing._rev)) {
               this.items[docKey] = doc;
            }
         }
      }
   }

   async uncache(selector) {
      const compiledSelector = compileDocumentSelector(selector);
      
      Object.keys(this.items).forEach(docKey => {
         if (!this.upserts[docKey] && compiledSelector(this.items[docKey])) {
            delete this.items[docKey];
         }
      });
   }

   async uncacheList(docKeys) {
      docKeys.forEach(docKey => {
         if (!this.upserts[docKey] && this.items[docKey]) {
            delete this.items[docKey];
         }
      });
   }

}


export default MemoryDb;
export { MemoryCollection };