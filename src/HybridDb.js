import EJSON from './EJSON';
import {
   normalizeUpsert,
   processFind
} from "./utils";




// Bridges a local and remote database, querying from the local first and then 
// getting the remote. Also uploads changes from local to remote.
class HybridDb {

   collections = {};
   
   constructor(localDb, remoteDb) {
      if (!localDb) {
         throw Error('Missing local database');
      } else if (!remoteDb) {
         throw Error('Missing remote database');
      }

      this.localDb = localDb;
      this.remoteDb = remoteDb;
   }

   async addCollection(name, options) {
      if (name === 'collections' || name === 'localDb' || name === 'remoteDb') {
         throw Error('Illegal operation on HybridDb instance');
      }

      const {
         localOptions = {},
         remoteOptions = {},
         ...hybridOptions
      } = options || {};

      if (!this.localDb.collections[name]) {
         await this.localDb.addCollection(name, localOptions);
      }
      if (!this.remoteDb.collections[name]) {
         await this.remoteDb.addCollection(name, remoteOptions);
      }

      const collection = new HybridCollection(name, this.localDb.collections[name], this.remoteDb.collections[name], hybridOptions);
      this[name] = collection;
      this.collections[name] = collection;
      return collection;
   }

   async removeCollection(name) {
      if (name === 'collections' || name === 'localDb' || name === 'remoteDb') {
         throw Error('Illegal operation on HybridDb instance');
      }

      await this.localDb.removeCollection(name);
      await this.remoteDb.removeCollection(name);

      delete this[name];
      delete this.collections[name];
   }

   getCollectionNames() {
      return Object.keys(this.collections);
   }

}



class HybridCollection {

   options = {
      cacheFind: true,       // Cache find results in local db
      cacheFindOne: true,    // Cache findOne results in local db
      interim: true,         // Return interim results from local db while waiting for remote db (see onRemoteData)
      useLocalOnRemoteError: true,  // Use local results if the remote find fails. Only applies if interim is false.
      shortcut: false,       // true to return `findOne` results if any matching result is found in the local database. Useful for documents that change rarely.
      timeout: 0,            // Set to ms to timeout in for remote calls
      sortUpserts: null,     // Compare function to sort upserts sent to server. (called by Array.sort)
      onRemoteData: null,    // called if interim is true and remote data updates local collection
      onRemoteError: null    // called if interim is true and remote find throws an error
   }

   constructor(name, localCol, remoteCol, options) {
      this.name = name;
      this.localCol = localCol;
      this.remoteCol = remoteCol;
      this.options = Object.assign(this.options, options || {});
   }


   async find(selector, options) {
      options = Object.assign({}, this.options, options || {});

      const upserts = await this.localCol.pendingUpserts();
      const removes = await this.localCol.pendingRemoves();

      const step2 = async localData => {
         // Setup remote options
         const remoteOptions = EJSON.clone(options);

         // If caching, get all fields
         if (options.cacheFind) {
            delete remoteOptions.fields;
         }

         // Add localData to options for remote find for quickfind protocol
         remoteOptions.localData = localData;

         // Setup timer variables
         let timer = null;
         let timedOut = false;

         return new Promise((resolve, reject) => {
            const localKey = this.localCol.options.key;

            const remoteSuccess = remoteData => {
               // Cancel timer
               if (timer) {
                  clearTimeout(timer);
               }

               // Ignore if timed out, caching asynchronously
               if (timedOut) {
                  if (options.cacheFind) {
                     this.localCol.cache(remoteData, selector, options);
                  }
               } else if (options.cacheFind) {
                  // Exclude any recent upserts/removes to prevent race condition
                  const cacheOptions = Object.assign({}, options, { exclude: removes.concat( upserts.map(u => u.doc[localKey]) ) });

                  this.localCol.cache(remoteData, selector, cacheOptions).then(() => {

                     // cache success, fetch local rows...
                     return this.localCol.find(selector, options);
                  }).then(resolve, reject);
               } else {
                  // Remove local remotes
                  let data = remoteData;

                  if (removes.length) {
                     const removeSet = new Set(removes);
                     
                     data = data.filter(doc => !removeSet.has(doc[localKey]));
                  }

                  // re-insert upserts
                  if (upserts.length) {
                     const upsertSet = new Set(upserts.map(u => u.doc[localKey]));

                     data = data.filter(doc => !upsertSet.has(doc[localKey]));
                     
                     data = data.concat(upserts.map(u => u.doc));

                     // Refilter/sort/limit
                     data = processFind(data, selector, options);
                  }

                  resolve(data);
               }
            };

            const remoteError = error => {
               // Cancel timer
               if (timer) {
                  clearTimeout(timer);
               }
               // If not timed-out and no interim, do local find
               if (!timedOut && !options.interim) {
                  if (options.useLocalOnRemoteError) {
                     resolve(localData);
                  } else {
                     reject(error);
                  }
               }
            };

            if (options.timeout) {
               timer = setTimeout(() => {
                  timer = null;
                  timedOut = true;
    
                  if (options.interim) {
                     if (options.useLocalOnRemoteError) {
                        resolve( this.localCol.find(selector, options) );
                     } else {
                        reject(new Error('Remote time out'));
                     }
                  } else {
                     // fallback to local data
                     resolve(localData);
                  }
               }, options.timeout);
            }

            this.remoteCol.find(selector, remoteOptions).then(remoteSuccess, remoteError);
         });
      };

      return this.localCol.find(selector, options).then(localData => {
         if (options.interim) {
            setTimeout(() => step2(localData).then(remoteData => {
               if (options.onRemoteData && !EJSON.equals(localData, remoteData)) {
                  options.onRemoteData(remoteData);
               }
            }, error => {
               if (options.onRemoteError) {
                  options.onRemoteError(error);
               }
            }), 0);

            return localData;
         } else {
            return step2(localData);
         }
      });
   }


   async findOne(selector, options) {
      options = Object.assign({}, this.options, options || {});

      const step2 = async localDoc => {
         const localKey = this.localCol.options.key;
         const findOptions = EJSON.clone(options);
         findOptions.interim = false;
         findOptions.useLocalOnRemoteError = false;
         findOptions.cacheFind = options.cacheFindOne;
         if (selector[localKey]) {
            findOptions.limit = 1;
         } else {
            // Without _id specified, interaction between local and remote changes is complex
            // For example, if the one result returned by remote is locally deleted, we have no fallback
            // So instead we do a find with no limit and then take the first result, which is very inefficient
            delete findOptions.limit;
         }

         return this.find(selector, findOptions).then(data => {
            if (data.length > 0 && !EJSON.equals(localDoc, data[0])) {
               return data[0];
            } else {
               // If nothing found, always report it, as interim find doesn't return null
               return localDoc;
            }
         })
      };

      const localDoc = options.interim || options.shortcut ? await this.localCol.findOne(selector, options) : null;

      if (localDoc) {
         if (!options.shortcut) {
            
            setTimeout(() => step2(localDoc).then(doc => {
               if (options.onRemoteData && !EJSON.equals(localDoc, doc)) {
                  options.onRemoteData([doc]);
               }
            }, error => {
               if (options.onRemoteError) {
                  options.onRemoteError(error);
               }
            }), 0);
         }

         return localDoc;
      } else {
         return step2();
      }
   }

   async upsert(docs, bases) {
      const items = normalizeUpsert(docs, bases, this.localCol.options.key, this.localCol.options.keyUtil);
      const results = await this.localCol.upsert( items.map(item => item.doc), items.map(item => item.base) );

      return Array.isArray(docs) ? results : results[0];
   }

   async remove(docKey) {
      return this.localCol.remove(docKey);
   }

   async upload() {
      const localKey = this.localCol.options.key;
      const updateMap = {
         doc: [],
         base: []
      };
  
      const uploadUpserts = async () => {
         const upserts = await this.localCol.pendingUpserts();
  
         if (this.options.sortUpserts) {
            upserts.sort((u1, u2) => this.options.sortUpserts(u1.doc, u2.doc));
         }
   
         if (upserts && upserts.length) {
            const resolveUpserts = [];
            const resolveRemoves = [];
  
            for (const upsert of upserts) {
               if (upsert) {
                  try {
                     const remoteDoc = await this.remoteCol.upsert(upsert.doc, upsert.base);
                     
                     if (remoteDoc) {
                        if (upsert.doc[localKey] !== remoteDoc[localKey]) {

                           updateMap.doc.push({ [localKey]: remoteDoc[localKey] });
                           updateMap.base.push({ [localKey]: upsert.doc[localKey] });

                           await this.localCol.upsert(remoteDoc);
                           resolveUpserts.push({ doc: remoteDoc });

                           // cleanup
                           await this.localCol.remove(upsert.doc[localKey]);
                           resolveRemoves.push(upsert.doc[localKey])
                        } else {
                           resolveUpserts.push(upsert);
                        }
                     } else {
                        await this.localCol.remove(upsert.doc[localKey]);
                     }
                  } catch (error) {
                     // If 410 error or 403, remove document
                     if (error.status === 410 || error.status === 403) {
                        await this.localCol.remove(upsert.doc[localKey]);
  
                        resolveRemoves.push(upsert.doc[localKey]);
  
                        // stop if was not 410
                        if (error.status !== 410) {
                           throw error;
                        }
                     } else {
                        throw error;
                     }
                  }
               }
            }
  
            if (resolveUpserts.length) {
               await this.localCol.resolveUpserts(resolveUpserts);
            }
            if (resolveRemoves.length) {
               await this.localCol.resolveRemoves( resolveRemoves );
            }
         }
      };
  
      const uploadRemoves = async () => {
         const removes = await this.localCol.pendingRemoves();
  
         if (removes && removes.length) {
            const resolveRemoves = [];
  
            for (const remove of removes) {
               if (remove) {
                  try {
                     await this.remoteCol.remove(remove);
  
                     resolveRemoves.push(remove);
                  } catch (error) {
                     // If 410 error or 403, remove document
                     if (error.status === 410 || err.status === 403) {
  
                        resolveRemoves.push(remove);
  
                        // stop if was not 410
                        if (error.status !== 410) {
                           throw error;
                        }
                     } else {
                        throw error;
                     }
                  }
               }
            }
  
            if (resolveRemoves.length) {
               await this.localCol.resolveRemoves( resolveRemoves );
            }
         }
      };
  
      await uploadUpserts();
      await uploadRemoves();

      return updateMap;
   }
   
}



export default HybridDb;
export { HybridCollection };