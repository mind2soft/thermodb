import EJSON from './EJSON';


// Like _.isArray, but doesn't regard polyfilled Uint8Arrays on old browsers as
// arrays.
const isArray = x => Array.isArray(x) && !EJSON.isBinary(x);
const _anyIfArray = (x, f) => isArray(x) ? x.some(f) : f(x);
const _anyIfArrayPlus = (x, f) => f(x) || (isArray(x) && x.some(f));


const isObject = x => x && 'object|function'.indexOf(typeof x) >= 0;


// ref: LocalCollection._f._type
const getObjectType = v => {
   if (typeof v === "number")
      return 1;
   if (typeof v === "string")
      return 2;
   if (typeof v === "boolean")
      return 8;
   if (isArray(v))
      return 4;
   if (v === null)
      return 10;
   if (v instanceof RegExp)
      return 11;
   if (typeof v === "function")
      // note that typeof(/x/) === "function"
      return 13;
   if (v instanceof Date)
      return 9;
   if (EJSON.isBinary(v))
      return 5;
   return 3; // object

   // XXX support some/all of these:
   // 14, symbol
   // 15, javascript code with scope
   // 16, 18: 32-bit/64-bit integer
   // 17, timestamp
   // 255, minkey
   // 127, maxkey
};


// ref: LocalCollection._f._typeorder
const typeOrder = [
   -1,  // (not a type)
   1,   // number
   2,   // string
   3,   // object
   4,   // array
   5,   // binary
   -1,  // deprecated
   6,   // ObjectID
   7,   // bool
   8,   // Date
   0,   // null
   9,   // RegExp
   -1,  // deprecated
   100, // JS code
   2,   // deprecated (symbol)
   100, // JS code
   1,   // 32-bit int
   8,   // Mongo timestamp
   1    // 64-bit int
];
// http://www.mongodb.org/display/DOCS/What+is+the+Compare+Order+for+BSON+Types
// XXX what is the correct sort position for Javascript code?
// ('100' in the matrix below)
// XXX minkey/maxkey
const getTypeOrder = t => typeOrder[t];


// deep equality test: use for literal document and array matches
// ref: LocalCollection._f._equal
const areObjectsEqual = (a, b) => EJSON.equals(a, b, { keyOrderSensitive: true });



// ref: LocalCollection._f._cmp
const compareObjects = (a, b) => {

   if (a === undefined)
      return b === undefined ? 0 : -1;
   if (b === undefined)
      return 1;

   var ta = getObjectType(a);
   var tb = getObjectType(b);
   var oa = getTypeOrder(ta);
   var ob = getTypeOrder(tb);

   if (oa !== ob)
      return oa < ob ? -1 : 1;

   if (ta !== tb) {
      // XXX need to implement this if we implement Symbol or integers, or
      // Timestamp
      throw Error("Missing type coercion logic in _cmp");

   } else if (ta === 7) { // ObjectID
      // Convert to string.
      ta = tb = 2;
      a = a.toHexString();
      b = b.toHexString();

   } else if (ta === 9) { // Date
      // Convert to millis.
      ta = tb = 1;
      a = a.getTime();
      b = b.getTime();

   } else if (ta === 1) { // double
      return a - b;

   } else if (tb === 2) { // string
      return a < b ? -1 : (a === b ? 0 : 1);

   } else if (ta === 3) { // Object
      // this could be much more efficient in the expected case ...
      const to_array = obj => {
         const ret = [];

         for (var key in obj) {
            ret.push(key);
            ret.push(obj[key]);
         }

         return ret;
      };

      return compareObjects(to_array(a), to_array(b));
   } else if (ta === 4) { // Array
      for (let i = 0; ; ++i) {
         if (i === a.length) {
            return (i === b.length) ? 0 : -1;
         } else if (i === b.length) {
            return 1;
         }

         const s = compareObjects(a[i], b[i]);

         if (s) {
            return s;
         }
      }
   } else if (ta === 5) { // binary
      // Surprisingly, a small binary blob is always less than a large one in
      // Mongo.
      if (a.length !== b.length) {
         return a.length - b.length;
      }

      for (let i = 0, len = a.length; i < len; ++i) {
         if (a[i] < b[i]) {
            return -1;
         } else if (a[i] > b[i]) {
            return 1;
         }
      }

      return 0;
   } else if (ta === 8) { // boolean
      if (a) {
         return b ? 0 : 1;
      } else {
         return b ? -1 : 0;
      }
   } else if (ta === 10) { // null
      return 0;
   } else if (ta === 11) { // regexp
      throw Error("Sorting not supported on regular expression"); // XXX
      // 13: javascript code
      // 14: symbol
      // 15: javascript code with scope
      // 16: 32-bit integer
      // 17: timestamp
      // 18: 64-bit integer
      // 255: minkey
      // 127: maxkey
   } else if (ta === 13) { // javascript code
      throw Error("Sorting not supported on Javascript code"); // XXX
   } else {
      throw Error("Unknown type to sort");
   }
};

// _makeLookupFunction(key) returns a lookup function.
//
// A lookup function takes in a document and returns an array of matching
// values.  This array has more than one element if any segment of the key other
// than the last one is an array.  ie, any arrays found when doing non-final
// lookups result in this function "branching"; each element in the returned
// array represents the value found at this branch. If any branch doesn't have a
// final value for the full key, its element in the returned list will be
// undefined. It always returns a non-empty array.
//
// _makeLookupFunction('a.x')({a: {x: 1}}) returns [1]
// _makeLookupFunction('a.x')({a: {x: [1]}}) returns [[1]]
// _makeLookupFunction('a.x')({a: 5})  returns [undefined]
// _makeLookupFunction('a.x')({a: [{x: 1},
//                                 {x: [2]},
//                                 {y: 3}]})
//   returns [1, [2], undefined]
// ref: LocalCollection._makeLookupFunction = function (key) { ... }
const createLookupFunction = key => {
   const dotLocation = key.indexOf('.');
   let first, lookupRest, nextIsNumeric;

   if (dotLocation === -1) {
      first = key;
   } else {
      first = key.substr(0, dotLocation);
      let rest = key.substr(dotLocation + 1);
      lookupRest = createLookupFunction(rest);
      // Is the next (perhaps final) piece numeric (ie, an array lookup?)
      nextIsNumeric = /^\d+(\.|$)/.test(rest);
   }

   return doc => {
      if (doc == null) { // null or undefined
         return [undefined];
      }

      let firstLevel = doc[first];

      // We don't "branch" at the final level.
      if (!lookupRest) {
         return [firstLevel];
      }

      // It's an empty array, and we're not done: we won't find anything.
      if (isArray(firstLevel) && firstLevel.length === 0) {
         return [undefined];
      }

      // For each result at this level, finish the lookup on the rest of the key,
      // and return everything we find. Also, if the next result is a number,
      // don't branch here.
      //
      // Technically, in MongoDB, we should be able to handle the case where
      // objects have numeric keys, but Mongo doesn't actually handle this
      // consistently yet itself, see eg
      // https://jira.mongodb.org/browse/SERVER-2898
      // https://github.com/mongodb/mongo/blob/master/jstests/array_match2.js
      if (!isArray(firstLevel) || nextIsNumeric) {
         firstLevel = [firstLevel];
      }

      return Array.prototype.concat.apply([], firstLevel.map(lookupRest));
   };
};


const hasOperators = valueSelector => {
   let theseAreOperators = undefined;
   for (const selKey in valueSelector) {
      const thisIsOperator = selKey.substr(0, 1) === '$';
      if (theseAreOperators === undefined) {
         theseAreOperators = thisIsOperator;
      } else if (theseAreOperators !== thisIsOperator) {
         throw new Error("Inconsistent selector: " + valueSelector);
      }
   }
   return !!theseAreOperators;  // {} has no operators
};

const compileValueSelector = valueSelector => {
   if (valueSelector == null) {  // undefined or null
      return value => _anyIfArray(value, x => x == null);  // undefined or null
   } else if (!isObject(valueSelector)) { // Selector is a non-null primitive (and not an array or RegExp either).
      return value => _anyIfArray(value, x => x === valueSelector);
   } else if (valueSelector instanceof RegExp) {
      return value => value !== undefined ? _anyIfArray(value, x => valueSelector.test(x)) : false;
   } else if (isArray(valueSelector)) { // Arrays match either identical arrays or arrays that contain it as a value.
      return value => isArray(value) ? _anyIfArrayPlus(value, x => areObjectsEqual(valueSelector, x)) : false;
   } else if (hasOperators(valueSelector)) {  // It's an object, but not an array or regexp.
      const operatorFunctions = [];

      Object.keys(valueSelector).forEach(operator => {
         const operand = valueSelector[operator];

         if (!(operator in VALUE_OPERATORS)) {
            throw Error(`Unrecognized operator: ${operator}`);
         }

         operatorFunctions.push(VALUE_OPERATORS[operator](operand, valueSelector.$options));
      });

      return value => operatorFunctions.every(f => f(value));
   } else {
      // It's a literal; compare value (or element of value array) directly to the
      // selector.
      return value => _anyIfArray(value, x => areObjectsEqual(valueSelector, x));
   }
};




// XXX can factor out common logic below
const LOGICAL_OPERATORS = {
   "$and": subSelector => {
      if (!subSelector || !isArray(subSelector) || !subSelector.length) {
         throw Error("$and/$or/$nor must be nonempty array");
      }
      const subSelectorFunctions = subSelector.map(compileDocumentSelector);

      return doc => subSelectorFunctions.every(f => f(doc));
   },

   "$or": subSelector => {
      if (!subSelector || !isArray(subSelector) || !subSelector.length) {
         throw Error("$and/$or/$nor must be nonempty array");
      }
      const subSelectorFunctions = subSelector.map(compileDocumentSelector);

      return doc => subSelectorFunctions.some(f => f(doc));
   },

   "$nor": subSelector => {
      if (!subSelector || !isArray(subSelector) || !subSelector.length) {
         throw Error("$and/$or/$nor must be nonempty array");
      }
      const subSelectorFunctions = subSelector.map(compileDocumentSelector);

      return doc => subSelectorFunctions.every(f => !f(doc));
   },

   "$where": selectorValue => {
      if (!(selectorValue instanceof Function)) {
         selectorValue = Function("return " + selectorValue);
      }

      return doc => selectorValue.call(doc);
   }
};

const VALUE_OPERATORS = {
   "$in": operand => {
      if (!isArray(operand)) {
         throw new Error("Argument to $in must be array");
      }

      const index = new Set(operand.filter(op => typeof op === 'string' || !isNaN(op)));

      return value => _anyIfArrayPlus(value, x => index.has(x) || operand.some(operandElt => areObjectsEqual(operandElt, x)));
   },

   "$all": operand => {
      if (!isArray(operand)) {
         throw new Error("Argument to $all must be array");
      }

      return value => isArray(value) && operand.every(operandElt => value.some(valueElt => areObjectsEqual(operandElt, valueElt)));
   },

   "$lt": operand => value => _anyIfArray(value, x => compareObjects(x, operand) < 0),

   "$lte": operand => value => _anyIfArray(value, x => compareObjects(x, operand) <= 0),

   "$gt": operand => value => _anyIfArray(value, x => compareObjects(x, operand) > 0),

   "$gte": operand => value => _anyIfArray(value, x => compareObjects(x, operand) >= 0),

   "$ne": operand => value => !_anyIfArrayPlus(value, x => areObjectsEqual(x, operand)),

   "$nin": operand => {
      if (!isArray(operand)) {
         throw new Error("Argument to $nin must be array");
      }

      const inFunction = VALUE_OPERATORS.$in(operand);

      return value => (value !== undefined) && !inFunction(value);
   },

   "$exists": operand => value => Boolean(operand) === (value !== undefined),

   "$mod": operand => {
      const divisor = operand[0];
      const remainder = operand[1] || 0;

      return value => _anyIfArray(value, x => x % divisor === remainder);
   },

   "$size": operand => value => isArray(value) && operand === value.length,

   "$type": operand => value => {
      // A nonexistent field is of no type.
      if (value === undefined) {
         return false;
      }
      // Definitely not _anyIfArrayPlus: $type: 4 only matches arrays that have
      // arrays as elements according to the Mongo docs.
      return _anyIfArray(value, x => getObjectType(x) === operand);
   },

   "$regex": (operand, options) => {
      if (options !== undefined) {
         // Options passed in $options (even the empty string) always overrides
         // options in the RegExp object itself.

         // Be clear that we only support the JS-supported options, not extended
         // ones (eg, Mongo supports x and s). Ideally we would implement x and s
         // by transforming the regexp, but not today...
         if (/[^gim]/.test(options)) {
            throw new Error("Only the i, m, and g regexp options are supported");
         }

         const regexSource = operand instanceof RegExp ? operand.source : operand;

         operand = new RegExp(regexSource, options);
      } else if (!(operand instanceof RegExp)) {
         operand = new RegExp(operand);
      }

      return value => (value !== undefined) && _anyIfArray(value, x => operand.test(x));
   },

   // evaluation happens at the $regex function above
   "$options": (/*operand*/) => (/*value*/) => true,

   "$elemMatch": operand => {
      const matcher = compileDocumentSelector(operand);

      return value => isArray(value) && value.some(x => matcher(x));
   },

   "$not": operand => {
      const matcher = compileValueSelector(operand);

      return value => !matcher(value);
   },


   // Always returns true. Must be handled in post-filter/sort/limit
   "$near": (/*operand*/) => (/*value*/) => true,

   // Always returns true. Must be handled in post-filter/sort/limit
   "$geoIntersects": (/*operand*/) => (/*value*/) => true,

};



// ----------------------------------------------------


// The main compilation function for a given selector.
const compileDocumentSelector = docSelector => {
   const perKeySelectors = [];
   const keys = docSelector ? Object.keys(docSelector) : [];

   keys.forEach(key => {
      const subSelector = docSelector[key];

      if (key.substr(0, 1) === '$') {
         if (!(key in LOGICAL_OPERATORS)) {
            throw new Error(`Unrecognized logical operator: ${key}`);
         }

         perKeySelectors.push(LOGICAL_OPERATORS[key](subSelector));
      } else {
         const lookUpByIndex = createLookupFunction(key);
         const valueSelectorFunc = compileValueSelector(subSelector);


         // We apply the selector to each "branched" value and return true if any
         // match. This isn't 100% consistent with MongoDB; eg, see:
         // https://jira.mongodb.org/browse/SERVER-8585
         perKeySelectors.push(doc => lookUpByIndex(doc).some(valueSelectorFunc));
      }
   });

   return doc => perKeySelectors.every(f => f(doc));
};



const compileSort = spec => {
   const sortSpecParts = [];

   if (isArray(spec)) {
      for (const specElement of spec) {
         if (typeof specElement === 'string') {
            sortSpecParts.push({
               lookup: createLookupFunction(specElement),
               ascending: true
            });
         } else {
            sortSpecParts.push({
               lookup: createLookupFunction(specElement[0]),
               ascending: specElement[1] !== 'desc'
            });
         }
      }
   } else if (typeof spec === 'object') {
      for (const key in spec) {
         sortSpecParts.push({
            lookup: createLookupFunction(key),
            ascending: spec[key] >= 0
         });
      }
   } else {
      throw Error(`Bad sort specification: ${JSON.stringify(spec)}`);
   }

   if (!sortSpecParts.length) {
      return () => 0;
   }

   const reduceValue = (branchValues, findMin) => {
      let reducted;
      let first = true;

      branchValues.forEach(branchValue => {
         if (!isArray(branchValue)) {
            branchValue = [branchValue];
         } else if (!branchValue.length) {
            branchValue = [undefined];
         }

         branchValue.forEach(value => {
            if (first) {
               reducted = value;
               first = false;
            } else {
               const cmp = compareObjects(reducted, value);
               if ((findMin && (cmp > 0)) || (!findMin && (cmp < 0))) {
                  reducted = value;
               }
            }
         });
      });

      return reducted;
   }

   return (a, b) => {
      for (const specPart of sortSpecParts) {
         var aValue = reduceValue(specPart.lookup(a), specPart.ascending);
         var bValue = reduceValue(specPart.lookup(b), specPart.ascending);
         var compare = compareObjects(aValue, bValue);
         if (compare) {
            return specPart.ascending ? compare : -compare;
         }
      }
   };
}





export {
   compileSort,
   compileDocumentSelector,
};