const EJSON = {};


EJSON.isBinary = obj => (typeof Uint8Array !== 'undefined' && obj instanceof Uint8Array) || (obj && obj.$Uint8ArrayPolyfill);

EJSON.equals = (a, b, options) => {
   const keyOrderSensitive = !!(options && options.keyOrderSensitive);

   if (a === b) {
      return true;
   } else if (!a || !b) { // if either one is falsy, they'd have to be === to be equal
      return false;
   } else if (!(typeof a === 'object' && typeof b === 'object')) {
      return false;
   } else if (a instanceof Date && b instanceof Date) {
      return a.valueOf() === b.valueOf();
   } else if (EJSON.isBinary(a) && EJSON.isBinary(b)) {
      if (a.length !== b.length) {
         return false;
      }
      for (let i = 0, len = a.length; i < len; ++i) {
         if (a[i] !== b[i]) {
            return false;
         }
      }
      return true;
   } else if (typeof (a.equals) === 'function') {
      return a.equals(b, options);
   } else if (a instanceof Array) {
      if (!(b instanceof Array)) {
         return false;
      } else if (a.length !== b.length) {
         return false;
      }
      for (let i = 0, len = a.length; i < len; ++i) {
         if (!EJSON.equals(a[i], b[i], options)) {
            return false;
         }
      }
      return true;
   } else {
      // fall back to structural equality of objects
      var ret;
   
      if (keyOrderSensitive) {
         const aKeys = Object.keys(a);
         const bKeys = Object.keys(b);

         return (aKeys.length === bKeys.length) && aKeys.every((aKey, index) => aKey === bKeys[index] && EJSON.equals(a[aKey], b[bKeys[index]], options));
      } else {
         return Object.keys(a).every(aKey => (aKey in b) && EJSON.equals(a[aKey], b[aKey], options));
      }
   }
};

EJSON.clone = v => {
   if (v === null || typeof v !== "object") {
      return v;
   } else if (v instanceof Date) {
      return new Date(v.getTime());
   } else if (EJSON.isBinary(v)) {
      const ret = EJSON.newBinary(v.length);
      for (var i = 0, len = v.length; i < len; ++i) {
         ret[i] = v[i];
      }
      return ret;
   } else if (Array.isArray(v) || (Object.prototype.toString.call(v) === '[object Arguments]')) {
      // For some reason, _.map doesn't work in this context on Opera (weird test
      // failures).
      const ret = [];
      for (let i = 0, len = v.length; i < len; ++i) {
         ret[i] = EJSON.clone(v[i]);
      }
      return ret;
   } else if (typeof v.clone === 'function') {
      // handle general user-defined typed Objects if they have a clone method
      return v.clone();
   } else {
      return v ? Object.keys(v).reduce((clone, key) => (clone[key] = EJSON.clone(v[key]), clone), {}) : v;
   }
};


export default EJSON;