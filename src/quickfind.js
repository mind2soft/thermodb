import sha1 from 'js-sha1';

import { compileSort } from './selector';

/**
Quickfind protocol allows sending information about which rows are already present locally to minimize 
network traffic.
Protocal has 3 phases:
encodeRequest: Done on client. Summarize which rows are already present locally by sharding and then hashing _id:_rev|
encodeResponse: Done on server. Given complete server list and results of encodeRequest, create list of changes, sharded by first two characters of _id
decodeResponse: Done on client. Given encoded response and local list, recreate complete list from server.
Interaction of sort, limit and fields:
- fields present: _rev might be missing. Do not use quickfind
- limit with no sort: This gives unstable results. Do not use quickfind
- sort: final rows need to be re-sorted. Since fields not present, is possible.
- no sort, no limit: always sort by _id
**/

// Characters to shard by of _id
const shardLength = 2


const groupByKey = (rows, key) => rows.reduce((groups, row) => {
   const groupKey = row[key].substr(0, shardLength);
   groups[groupKey] = groups[groupKey] || [];
   groups[groupKey].push(row);
   return groups;
}, {});



const hashRows = (rows, key) => {
   const hash = sha1.create();
   for (let row of rows.sort((a, b) => String(a[key]).localeCompare(String(b[key])))) {
      hash.update(row[key] + ':' + (row._rev || '') + '|');
   }

   // 80 bits is enough for uniqueness
   return hash.hex().substr(0, 20);
};



// Given an array of client rows, create a summary of which rows are present
const encodeRequest = (clientRows, key) => {
   // Index by shard
   clientRows = groupByKey(clientRows, key);

   // Hash each one
   const request = Object.keys(clientRows).reduce((request, rowKey) => (request[rowKey] = hashRows(clientRows[rowKey], key), request), {});

   return request;
};



// Given an array of rows on the server and an encoded request, create encoded response
const encodeResponse = (serverRows, encodedRequest, key) => {
   // Index by shard
   serverRows = groupByKey(serverRows, key);

   // Include any that are in encoded request but not present
   Object.keys(encodedRequest).forEach(encodedKey => serverRows[encodedKey] = serverRows[encodedKey] || []);

   // Only keep ones where different from encoded request
   //const response = _.pick(serverRows, (rows, key) -> hashRows(rows) != encodedRequest[key])
   const response = Object.keys(serverRows).reduce((obj, rowKey) => {
      if (hashRows(serverRows[rowKey], key) !== encodedRequest[rowKey]) {
         obj[rowKey] = serverRows[rowKey];
      }
      return obj;
   }, {});

   return response;
};

// Given encoded response and array of client rows, create array of server rows
const decodeResponse = (encodedResponse, clientRows, sort, key) => {
   // Index by shard
   clientRows = groupByKey(clientRows, key);

   // Overwrite with response
   let serverRows = Object.assign({}, clientRows, encodedResponse);

   // Flatten
   serverRows = Object.keys(serverRows).reduce((rows, groupKey) => {
      rows.push(...serverRows[groupKey]);
      return rows;
   }, []);

   // Sort
   if (sort) {
      serverRows.sort(compileSort(sort));
   } else {
      const collator = new Intl.Collator(undefined, {numeric: true, sensitivity: 'base'});

      serverRows = serverRows.sort((a, b) => collator.compare(a[key], b[key]));
   }

   return serverRows;
}



export { encodeRequest, encodeResponse, decodeResponse };