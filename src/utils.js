import hash from 'object-hash';
import booleanPointInPolygon from '@turf/boolean-point-in-polygon';
import intersect from '@turf/intersect';
import booleanCrosses from '@turf/boolean-crosses';
import booleanWithin from '@turf/boolean-within';

import EJSON from './EJSON';
import { 
   compileSort,
   compileDocumentSelector
} from './selector';


const D2R = Math.PI / 180;

let counter = 0;

const deg2rad = deg => deg * D2R;

const pointInPolygon = (point, polygon) => booleanPointInPolygon(point, polygon);

const polygonIntersection = (polygon1, polygon2) => intersect(polygon1, polygon2);


// -----------------------------------------------------


const processNearOperator = (selector, list) => {
   const keys = Object.keys(selector);

   for (const key of keys) {
      const value = selector[key];

      if (value && value['$near']) {
         const geo = value['$near']['$geometry'];

         if (!geo || geo.type !== 'Point') {
            break;
         }

         const minDistance = 0;
         const maxDistance = value['$near']['$maxDistance'] ? value['$near']['$maxDistance'] : Infinity;

         // Get distances
         list = list.filter(doc => doc[key] && doc[key].type === 'Point').map(doc => ({
            doc,
            distance: getDistanceFromLatLngInM(
               geo.coordinates[1], geo.coordinates[0],
               doc[key].coordinates[1], doc[key].coordinates[0]
            )
         })).filter(item => item.distance >= minDistance && item.distance <= maxDistance).sort((a, b) => a.distance - b.distance).map(item => item.doc);
      }
   }

   return list;
};


const processGeoIntersectsOperator = (selector, list) => {
   for (const key in selector) {
      const value = selector[key];

      if (value && value['$geoIntersects']) {
         const geo = value['$geoIntersects']['$geometry']

         // Can only test intersection with polygon
         if (geo.type === 'Polygon') {
            // Check within for each
            list = list.filter(doc => {
               
               if (!doc[key]) {
                  // Ignore if null
                  return false
               } else if (doc[key].type === 'Point') {
                  // Check point or polygon
                  return pointInPolygon(doc[key], geo)
               } else if (['Polygon', 'MultiPolygon'].indexOf(doc[key].type) >= 0) {
                  return polygonIntersection(doc[key], geo)
               } else if (doc[key].type === 'LineString') {
                  return booleanCrosses(doc[key], geo) || booleanWithin(doc[key], geo)
               } else if (doc[key].type == 'MultiLineString') {
                  // Bypass deficiencies in turf.js by splitting it up
                  for (let line of doc[key].coordinates) {
                     const lineGeo = { type: 'LineString', coordinates: line };

                     if (booleanCrosses(lineGeo, geo) || booleanWithin(lineGeo, geo)) {
                        return true
                     }
                  }

                  return false
               }
            });
         }
      }
   }
  
  return list
};


const D = 12741972; // Diameter of the earth in m

const getDistanceFromLatLngInM = (lat1, lng1, lat2, lng2) => {
  const dLat = Math.sin(deg2rad(lat2 - lat1) / 2);
  const dLng = Math.sin(deg2rad(lng2 - lng1) / 2);
  const dLat1 = Math.cos(deg2rad(lat1));
  const dLat2 = Math.cos(deg2rad(lat2));
  const a = dLat * dLat + dLat1 * dLat2 * dLng * dLng;

  return D * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
};






// ------------------------------------------------------


class KeyUtil {
   constructor(field) {
      this.field = field || 'id';
   }
   createKey() { throw Error('Not implemented'); }
   isKey(key) { 
      return key && (typeof key === 'string') || !isNaN(key);
   }
   getField() { return this.field }
}

KeyUtil.Hash256 = class KeyHash256 extends KeyUtil {
   createKey() {
      return hash({ t: Date.now(), r: Math.random(), c: counter++ }).substr(0, 32);
   }
};

KeyUtil.Serial = class KeySerial extends KeyUtil {
   prevTimestamp = null;
   sequence = 0;

   constructor(field, sequenceSize) {
      super(field);
      this.multiplier = Math.pow(10, Math.max(sequenceSize || 3, 1));
   }

   createKey() {
      const ts = Date.now();

      if (this.prevTimestamp !== ts) {
         this.sequence = 0;
         this.prevTimestamp = ts;
      }

      return (ts * this.multiplier) + (this.sequence++);
   }
};

KeyUtil.Default = KeyUtil.Hash256;



// ------------------------------------------------------




const normalizeUpsert = (docs, bases, key, keyUtil) => {
   if (!Array.isArray(docs)) {
      docs = [docs];
      bases = [bases];
   } else {
      bases = bases || [];
   }

   key = key || keyUtil.getField();

   const items = docs.map((doc, index) => ({ doc, base: index < bases.length ? bases[index] : undefined }));

   for (const item of items) {
      if (!item.doc[key]) {
         item.doc[key] = keyUtil.createKey();
      }
      if (item.base && !item.base[key]) {
         throw new Error(`Base needs ${key}`);
      } else if (item.base && String(item.base[key]) !== String(item.doc[key])) {
         throw new Error(`Base needs same ${key} for ${item.doc[key]}`);
      }
   }

   return items;
};



const processFind = (items, selector, options, key) => {
   let filtered = items.filter( compileDocumentSelector(selector) )

   // Handle geospatial operators
   filtered = processNearOperator(selector, filtered)
   filtered = processGeoIntersectsOperator(selector, filtered)

   if (options && options.sort) {
      filtered.sort( compileSort(options.sort) )
   }

   // TODO : optimize

   if (options && options.skip) {
      filtered = filtered.slice( options.skip );
   }

   if (options && options.limit) {
      filtered = filtered.slice( 0, options.limit );
   }

   // Apply fields if present
   if (options && options.fields) {
      filtered = filterFields(filtered, options.fields, key);
   }

   return filtered
};



const filterFields = (items, fields, key) => {
   const keys = Object.keys(fields || {});
   
   // Handle trivial case
   if (!keys.length) {
      return items
   }
   
   const includeFields = fields[keys[0]] === 1;

   if (includeFields) {
      const pathExist = (path, obj) => {
         let i = 0;
         let len = path.length;
   
         while (obj && (i < len)) {
            obj = obj[path[i++]];
         }
   
         return i === len;
      }
   
      keys.push( key );

      return items.map(item => {
         const newItem = {};
   
         for (const field of keys) {
            const path = field.split('.');
   
            if (pathExist(path, item)) {
               const lastPath = path.pop();
               let from = item;
               let to = newItem;
   
               for (const pathKey of path) {
                  to = to[pathKey] || (to[pathKey] = {});
                  from = from[pathKey];
               }
   
               to[lastPath] = from[lastPath];
            }
         }
   
         return newItem;
      });
   } else {
      return items.map(item => {
         const newItem = EJSON.clone(item);
   
         for (const field of keys) {
            const path = field.split('.');
            const lastPath = path.pop();
   
            let from = newItem;
            for (const pathKey of path) {
               from = from[pathKey];
   
               if (!from) {
                  break;
               }
            }
   
            if (from) {
               delete from[lastPath];
            }
         }
   
         return newItem;
      });
   }
};



export {
   KeyUtil,  // new API
   normalizeUpsert,
   processFind
};