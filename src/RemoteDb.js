import { default as defaultHttpClient } from './httpClient';
import * as quickfind from './quickfind';
import EJSON from './EJSON';

import { 
   KeyUtil,
   normalizeUpsert,
} from './utils';



const concatUrl = (url, suffix) => {
   url = url.trim();

   return url + (url.endsWith('/') ? '' : '/') + suffix;
};



class RemoteDb {

   collections = {};

   options = {
      keyUtil: null,
      url: '',
      client: null,
      httpClient: defaultHttpClient,
      httpOptions: null,
      useQuickFind: true,
      usePostFind: true,
   };

   constructor(options) {
      Object.assign(this.options, options || {});

      if (!this.options.keyUtil) {
         this.options.keyUtil = new KeyUtil.Default();
      }
   }

   async addCollection(name, options) {
      if (name === 'collections' || name === 'options') {
         throw Error('Illegal operation on RemoteDb instance');
      }

      const keyUtil = this.options.keyUtil;
      const {
         client = this.options.client,
         httpClient = this.options.httpClient,
         httpOptions = this.options.httpOptions,
         useQuickFind = this.options.usePostFind,
         usePostFind = this.options.usePostFind,
         key
      } = options || {};
      let url = this.options.url;

      if (Array.isArray(url)) {
         url = url.map(url => concatUrl(url, name));
      } else {
         url = concatUrl(url, name);
      }

      const collection = new RemoteCollection(name, { key, keyUtil, url, client, httpClient, httpOptions, useQuickFind, usePostFind });
      this[name] = collection;
      this.collections[name] = collection;
      return collection;
   }

   async removeCollection(name) {
      if (name === 'collections' || name === 'options') {
         throw Error('Illegal operation on RemoteDb instance');
      }
      
      delete this[name];
      delete this.collections[name];
   }

   getCollectionNames() {
      return Object.keys(this.collections);
   }

}



class RemoteCollection {

   // Options:  key, httpOptions, useQuickFind, usePostFind
   constructor(name, { url, client, httpClient, ...options }) {
      if (!options || !options.keyUtil) {
         throw Error('Missing keyUtil');
      }

      this.name = name;
      this.url = url;
      this.client = client;
      this.httpClient = httpClient || defaultHttpClient;
      this.options = options;

      if (!this.options.key) {
         this.options.key = this.options.keyUtil.getField();
      }
   }

   getUrl() {
      if (Array.isArray(this.url)) {
         const url = this.url.pop();
         // Add URL to the front of the array
         this.url.unshift(url);
         return url;
      } else {
         return this.url;
      }
   }

   async find(selector, options) {
      let searchMethod = 'get';

      options = options || {};

      if (this.useQuickFind && options.localData && (!options.fields || options.fields._rev) && !(options.limit && !options.sort && !options.orderByExprs)) {
         // Determine method: "get", "post" or "quickfind"
         // If in quickfind and localData present and (no fields option or _rev included) and not (limit with no sort), use quickfind
         searchMethod = "quickfind"
      } else if (this.options.usePostFind && JSON.stringify({ selector, sort: options.sort, fields: options.fields, params: options.params }).length > 500) {
         // If selector or fields or sort is too big, use post
         searchMethod = "post"
      }

      const url = this.getUrl();
      const params = options.params ? EJSON.clone(options.params) : {};
      const body = {
         selector: selector || {}
      };

      if (options.sort) {
         body.sort = options.sort;
      }
      if (options.limit) {
         body.limite = options.limit;
      }
      if (options.skip) {
         body.skip = options.skip;
      }
      if (options.fields) {
         body.fields = JSON.stringify(options.fields);
      }

      // Advanced options for mwater-expression-based filtering and ordering
      if (options.whereExpr) {
         body.whereExpr = JSON.stringify(options.whereExpr);
      }
      if (options.orderByExprs) {
         body.orderByExprs = JSON.stringify(options.orderByExprs)
      }

      if (this.client) {
         params.client = this.client;
      }

      // Add timestamp for Android 2.3.6 bug with caching
      if (typeof navigator !== 'undefined' && (navigator.userAgent.toLowerCase().indexOf('android 2.3') != -1)) {
         params._ = Date.now();
      }

      if (searchMethod === 'quickfind') {
         body.quickfind = quickfind.encodeRequest(options.localData)

         url = url + '/quickfind';
      } else if (searchMethod === 'post') {
         url = url + '/find';
      }

      return this.httpClient(searchMethod === 'get' ? 'GET' : 'POST', url, params, body, this.options.httpOptions);
   }


   async findOne(selector, options) {
      const params = options && options.params ? EJSON.clone(options.params) : {};

      params.selector = selector || {};

      if (options && options.sort) {
         body.sort = options.sort;
      }
      if (options && options.limit) {
         body.limite = options.limit;
      }
      if (this.client) {
         params.client = this.client;
      }

      // Add timestamp for Android 2.3.6 bug with caching
      if (typeof navigator !== 'undefined' && (navigator.userAgent.toLowerCase().indexOf('android 2.3') != -1)) {
         params._ = Date.now();
      }

      return this.httpClient('GET', this.getUrl(), params, null, this.options.httpOptions);
   }


   async upsert(docs, bases) {
      if (!this.client) {
         throw new Error("Client required to upsert")
      }

      const items = normalizeUpsert(docs, bases, this.options.key, this.options.keyUtil);

      // Check if bases present
      const basesPresent = items.map(item => item.base).filter(base => base).length > 0;
      const params = {
         client: this.client
      };

      // Add timestamp for Android 2.3.6 bug with caching
      if (typeof navigator !== 'undefined' && (navigator.userAgent.toLowerCase().indexOf('android 2.3') != -1)) {
         params._ = Date.now();
      }

      const method = basesPresent ? 'PATCH' : 'POST';

      // Handle single case
      if (items.length === 1) {
         const body = basesPresent ? items[0] : items[0].doc;
         const response = await this.httpClient(method, this.getUrl(), params, body, this.options.httpOptions)

         if (Array.isArray(docs)) {
            return [resposne];
         } else {
            return response;
         }
      } else {
         const body = basesPresent ? {
            doc: items.map(item => item.doc),
            base: items.map(item => item.base)
         } : items.map(item => item.doc);

         return this.httpClient(method, this.getUrl(), params, body, this.options.httpOptions);
      }
   }


   async remove(docKey) {
      if (!this.client) {
         throw new Error("Client required to upsert")
      }

      const params = { client: this.client };

      return this.httpClient('DELETE', this.getUrl() + '/' + docKey, params, null, this.options.httpOptions);
   }

}





export default RemoteDb;
export { RemoteCollection };