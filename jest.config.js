module.exports = {
   "testEnvironment": "jsdom",
   "testMatch": [ "**/test/**/?(*.)+(test).js" ],
   "coveragePathIgnorePatterns": ["/node_modules/", "/test/"],
   "setupFiles": [
      "fake-indexeddb/auto",
      'cross-fetch/polyfill'
   ]
};