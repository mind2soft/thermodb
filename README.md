# ThermoDb

A fork of [minimongo](https://github.com/mWater/minimongo) for agnostic datasources.


## Usage

```js
import MemoryDb from 'thermodb/MemoryDb';

const db = new MemoryDb();

const collection = await db.addCollection('things');
// collection === db.collections.things === db.things

const foo = await collection.upsert({ a: 'Hello' });
// -> { id: ..., a: 'Hello' }
```

## API

### HybridDb

Combines results from the local database with remote data.

```js
import HybridDb from 'thermodb/HybridDb';

const db = new IndexedDb(localDb, remoteDb);

await db.addCollection('foo', {
   // Cache find results in local db
   cacheFind: true,
   // Cache findOne results in local db
   cacheFindOne: true,
   // Return interim results from local db while waiting for remote db (see onRemoteData)
   interim: true,
   // Use local results if the remote find fails. Only applies if interim is false.
   useLocalOnRemoteError: true,
   // true to return `findOne` results if any matching result is found in the local database.
   // Useful for documents that change rarely.
   shortcut: false,
   // Set to ms to timeout in for remote calls
   timeout: 0,
   // Compare function to sort upserts sent to server. (called by Array.sort()
   sortUpserts: null,
   // called if interim is true and remote data updates local collection
   onRemoteData: null,
   // called if interim is true and remote find throws an error
   onRemoteError: null
});
```


### IndexedDb

Make a local database backed by IndexedDb:

```js
import IndexedDb from 'thermodb/IndexedDb';

const db = new IndexedDb({
   /*
   Optionally define the key attribute of the documents. Will default
   to KeyUtil.getField()
   */
   key: null,
   /*
   Optionally define the KeyUtil instance to generate and manage document ids
   */
   keyUtil: null,
   /*
   Optionally define a namespace to store data
   */
   namespace: 'default',
});

// all options override db options
await db.addCollection('foo', options);
```


### MemoryDb

Make an in-memory local database backed by a simple JavaScript object.

```js
import MemoryDb from 'thermodb/MemoryDb';

const db = new MemoryDb({
   /*
   Optionally define the key attribute of the documents. Will default
   to KeyUtil.getField()
   */
   key: null,
   /*
   Optionally define the KeyUtil instance to generate and manage document ids
   */
   keyUtil: null,
   /*
   Optionally define the strategy when returning documents:
   - "clone" (default) will deep clone the document before returning it
   - "freeze" will prevent any modification of the returned document
   */
   safety: "clone",
});

// all options override db options
await db.addCollection('foo', options);
```


### RemoteDb

Uses AJAX-JSON calls to an API to query a server-side database.

```js
import RemoteDb from 'thermodb/RemoteDb';

const db = new RemoteDb({
   /*
   Optionally define the name of the client that will be passed as argument
   to the API
   */
   client: null,
   /*
   Optionally define the KeyUtil instance to generate and manage document ids
   */
   keyUtil: null,
   /*
   The API URL to request and post data
   */
   url: '',
   /*
   Optionally define a function provding the implementation to send data to
   the API server
   */
   httpClient: defaultHttpClient,
   /*
   Optionally define the options to pass to the httpClient function
   */
   httpOptions: null,
   /*
   Does the API support QuickFind?
   */
   useQuickFind: true,
   /*
   Does the API support POST request?
   */
   usePostFind: true,
});

await db.addCollection('foo', {
   /*
   Optionally define the key attribute of the documents. Will default
   to KeyUtil.getField()
   */
   key: null
});
```

### Database instances

```js
await db.addCollection(name, options);
// -> a Collection instance

await db.removeCollection(name);
// -> undefined

db.getCollectionNames();
// -> Array<String>
```


### Collection instances

```js
db.collections['foo'] === db['foo'];
// -> true

await db.foo.find(selector, options);
// -> Array<Object>

await db.foo.findOne(selector, options);
// -> Object

await db.foo.upsert(docs);
// -> Object or Array<Object>

await db.foo.remove(docKey);
// -> mixed
```